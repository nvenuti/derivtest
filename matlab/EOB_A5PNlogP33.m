function [A, dA, d2A, D, dD, d2D] =EOB_A5PNlogP33(r,nu)
%EOB_A5PNlogP33 function EOB_A5PNlogP33(r,nu)
%   This function computes the Pade' (3,3) resummed A function (with its
%   derivatives) starting from the 5PN-expanded version of the A function
%   including 4PN and 5PN log terms.


global Dpn
global d5nu2_fit
global a6

% constants
nu2 = nu^2;
pi2 = pi^2;
pi4 = pi2*pi2;
pi6 = pi4*pi2;

EulerGamma =  0.57721566490153286061;
a5c0       = -4237/60 + 2275/512*pi2 + 256/5*log(2) + 128/5*EulerGamma;
a5c1       = -221/6   + 41/32*pi2;
a5         =  a5c0 + nu*a5c1;

% shortcuts
u    = 1./r;
u2   = u.^2;
u3   = u.^3;
u4   = u.^4;
logu = log(u);
ln2  = log(2);
ln3  = log(3);

a5tot = a5  + 64/5*logu;
a6tot = a6  + (-7004/105 - 144/5*nu)*logu;

%----------------------------------------------------------
% preliminary: coefficients of the Pade and its derivatives
%----------------------------------------------------------
N0     = -1737228288  + 3538944*a5tot + 7077888*nu + 142073856*pi2 - 2904768*pi4;
N1     = -27216576512 - 3538944*a6tot + 3338735616*pi2 - 136524096*pi4 + 1860867*pi6 + a5tot*(110886912 - 4534272*pi2);
D0     =  N0;
D1     = -18432*(192*a6tot + 6016*nu - 246*nu*pi2 + a5tot*(-3008 + 123*pi2));
D2     = -192*(9216*a5tot.^2 + 18432*a5tot*nu + (-3008 + 123*pi2)*(96*a6tot + nu*(3008 - 123*pi2)));
D3     =  nu*(-3538944*a6tot - 36864*a5tot*(-3008 + 123*pi2) + power(-3008 + 123*pi2,3));

%1st derivatives of the above coefficients entering danu/du
dN0 = 226492416./(5*u);
dN1 = (-7077888*(-8186 - 504*nu + 287*pi2))./(35.*u);
dD0 = dN0;
dD1 = (-1179648*(-4*(7015 + 756*nu) + 861*pi2))./(35.*u);
dD2 = (-24576*(4128768*logu + 5*(64512*a5 + 64*(82297 + 36540*nu) - 123*(1751 + 756*nu)*pi2)))./(175.*u);
dD3 = (-7077888*nu*(-8186 - 504*nu + 287*pi2))./(35.*u);

% 2nd derivatives of the coefficients
d2N0 = -226492416./(5.*u2);
d2N1 = -(-7077888*(-8186 - 504*nu + 287*pi2))./(35.*u2);
d2D0 = d2N0;
d2D1 = -(-1179648*(-4*(7015 + 756*nu) + 861*pi2))./(35.*u2);
d2D2 = -(-24576*(22206272 + 322560*a5 + 4128768*logu + 11692800*nu - 1076865*pi2 - 464940*nu*pi2))./(175.*u2);
d2D3 = -(-7077888*nu*(-8186 - 504*nu + 287*pi2))./(35.*u2);

% the small a(u,nu) function
Num  = N0 + N1.*u;
Den  = D0 + D1.*u + D2.*u2 + D3.*u3;
anu  = Num./Den;

% derivative of anu with respect to u

dNum    = dN0 + N1 + u.*dN1;
dDen    = dD0 + D1 + u.*dD1 + 2.*u.*D2 + u2.*dD2 + 3*u2.*D3 + u3.*dD3;
danu_du = anu.*(dNum./Num - dDen./Den);

% second derivative of anu with respect to u.
d2Num = d2N0 + 2*dN1 + u.*d2N1;
d2Den = u3.*d2D3 + u2.*(d2D2+6*dD3) + u.*(d2D1 + 4*dD2 + 6*D3) + d2D0 + 2*dD1 + 2*D2;
d2anu_du2 = anu./(Num.*Den).*(2.*dDen.^2.*anu - 2.*dNum.*dDen + Den.*d2Num - d2Den.*Num);

% derivatives of A with respect to u
dA_du  = -2 + 6*nu*u2.*anu + 2*nu*u3.*danu_du;
d2A_du = 12*nu*u.*anu + 12*nu*u2.*danu_du + 2*nu*u3.*d2anu_du2;


% the A function and  its derivatives with respect to r
A   =  1-2*u + 2*nu*u3.*anu;
dA  =  -u2.*dA_du;
d2A =   u4.*d2A_du + 2*u3.*dA_du;

switch Dpn
    
    %-----------------------
    % The D and dD functions
    %-----------------------
    case '4PN'
        %===================================================================
        %
        % 4PN accurate D function (with log). Resummed taking Pade(0,4)
        %
        %-------------------------------------------------------------------        
        d40 = -533/45 - 23761/1536*pi2 + 1184/15*EulerGamma - 6496/15*ln2 + 2916/5*ln3; %221.5719912
        d41 =  123/16*pi2 - 260; % -184.1274162
        d4  = d40 + nu*d41;
        
        % resummation of bar{D} taking the inverse
        Dp  = 1.0 + 6*nu*u2 - 2*nu*(3.0*nu-26.0)*u3 + nu*(d4 + 592/15*log(u)).*u4;
        D   = 1./Dp;
        dD  = 2*nu/15*u3.*(90 - 45*u*(3*nu-26) + u2.*(296+30*d4+1184*log(u))).*D.^2;
        d2D = 0;
        
    case '4PNpade'
        %==================================================================
        % Pade(2,2) of the 4PN-accurate D function (with logarithmic terms)
        %==================================================================
        
        d2    = -6*nu;
        d3    = -52*nu+6*nu2;
        c1    = 533/45 - 1184/15*EulerGamma + 23761/1536*pi2 +6496/15*ln2 - 2916/5*ln3;
        c2    = 296 - 123/16*pi2;
        d4c   = c1 + nu*c2;
        d4log = -592/15;
        d4    = nu*(d4c + d4log.*log(u));
        % its derivative
        Dd4   = nu.*d4log./u;
        D2d4  = -nu.*d4log./u2;
        
        N1  = -d3./d2;
        N2  =  (d2.^3 + d3.^2 - d2.*d4)./(d2.^2);
        dN2 =  -1/d2.*Dd4;
        d2N2=  -1/d2.*D2d4;
        D1  =  N1;
        D2  =  (d3.^2 - d2.*d4)./(d2.^2);
        dD2 =  -1/d2.*Dd4;
        d2D2=  -1/d2.*D2d4;
        
        Num = 1 + N1.*u + N2.*u2;
        Den = 1 + D1.*u + D2.*u2;
        
        % derivatives numerator and denominator with respect to u
        % NOTE: d4(u) because of the log!!
        dNum = N1+2*N2.*u + dN2.*u2;
        dDen = D1+2*D2.*u + dD2.*u2;
        d2Num = 2*N2 + 4*dN2.*u + d2N2.*u2;
        d2Den = 2*D2 + 4*dD2.*u + d2D2.*u2;
        
        % Finally, the D function and its derivative
        D  =  Num./Den;
        dD = -u2.*(dNum.*Den - Num.*dDen)./(Den.^2);
        d2D = -2*u.*dD + u4.*( (d2Num.*Den-Num.*d2Den)./(Den.^2)...
            -2.*dDen.*(dNum.*Den-Num.*dDen)./(Den.^3));
    case '5PNpade'
        
        d5nu2 = d5nu2_fit; % only analytically uncalculated 5PN coefficient set to zero        
        d2    = -6*nu;
        d3    = -52*nu+6*nu2;
        c1    = 533/45 - 1184/15*EulerGamma + 23761/1536*pi2 +6496/15*ln2 - 2916/5*ln3;
        c2    = 296 - 123/16*pi2;
        d4c   = c1 + nu*c2;
        d4log = -592/15;
        d4    = nu*(d4c + d4log.*log(u));
        % its derivative
        Dd4   = nu.*d4log./u;
        D2d4  = -nu.*d4log./u2;
        d5c   = (-294464/175)+(2840/7).*EulerGamma+(-120648/35).*ln2+(19683/7).* ...
                ln3+((-2216/105)+(-1).*d5nu2+(6784/15).*EulerGamma+(326656/21).* ...
                ln2+(-58320/7).*ln3).*nu+(63707/512).*pi2+nu2.*((-1285/3)+(205/16) ...
                .*pi2);
        d5log = (1420/7)+(3392/15).*nu;
        d5    = nu.*(d5c + d5log.*log(u));
        Dd5   = nu.*d5log./u;
        D2d5  = -nu.*d5log./u2;
        
        N1   = (d3.^2+(-1).*d2.*d4).^(-1).*((-1).*d3.*d4+d2.*d5);
        N2   = ((-1).*d3.^2+d2.*d4).^(-1).*((-1).*d2.*d3.^2+d2.^2.*d4+(-1).*d4.^2+d3.*d5);
        N3   = (d3.^2+(-1).*d2.*d4).^(-1).*(d3.^3+(-2).*d2.*d3.*d4+d2.^2.*d5);
        dN1  = (d3.^2+(-1).*d2.*d4).^(-2).*(((-1).*d3.^3+d2.^2.*d5).*Dd4+d2.*( ...
            d3.^2+(-1).*d2.*d4).*Dd5);
        dN2  = (d3.^2+(-1).*d2.*d4).^(-2).*((-1).*d2.*d4.^2.*Dd4+d3.*d4.*(2.*d3.* ...
            Dd4+d2.*Dd5)+(-1).*d3.*(d2.*d5.*Dd4+d3.^2.*Dd5));
        dN3  = d2.*(d3.^2+(-1).*d2.*d4).^(-2).*(((-1).*d3.^3+d2.^2.*d5).*Dd4+d2.* ...
            (d3.^2+(-1).*d2.*d4).*Dd5);
        d2N1 = (d3.^2+(-1).*d2.*d4).^(-3).*((-1).*(d3.^2+(-1).*d2.*d4).*(d2.* ...
            D2d5.*((-1).*d3.^2+d2.*d4)+D2d4.*(d3.^3+(-1).*d2.^2.*d5))+2.*((-1) ...
            .*d2.*d3.^3+d2.^3.*d5).*Dd4.^2+2.*d2.^2.*(d3.^2+(-1).*d2.*d4).* ...
            Dd4.*Dd5);
        d2N2 = (d3.^2+(-1).*d2.*d4).^(-3).*((-1).*(d3.^2+(-1).*d2.*d4).*(D2d5.* ...
            d3.^3+(-1).*d3.*(d2.*D2d5+2.*D2d4.*d3).*d4+d2.*D2d4.*d4.^2+d2.* ...
            D2d4.*d3.*d5)+2.*(d3.^4+(-1).*d2.^2.*d3.*d5).*Dd4.^2+2.*d2.*d3.*(( ...
            -1).*d3.^2+d2.*d4).*Dd4.*Dd5);
        d2N3 = d2.*(d3.^2+(-1).*d2.*d4).^(-3).*((-1).*(d3.^2+(-1).*d2.*d4).*(d2.* ...
            D2d5.*((-1).*d3.^2+d2.*d4)+D2d4.*(d3.^3+(-1).*d2.^2.*d5))+2.*((-1) ...
            .*d2.*d3.^3+d2.^3.*d5).*Dd4.^2+2.*d2.^2.*(d3.^2+(-1).*d2.*d4).* ...
            Dd4.*Dd5);
        D1   = (d3.^2+(-1).*d2.*d4).^(-1).*((-1).*d3.*d4+d2.*d5);
        D2   = ((-1).*d3.^2+d2.*d4).^(-1).*((-1).*d4.^2+d3.*d5);
        dD1  = (d3.^2+(-1).*d2.*d4).^(-2).*(((-1).*d3.^3+d2.^2.*d5).*Dd4+d2.*( ...
            d3.^2+(-1).*d2.*d4).*Dd5);
        dD2  = (d3.^2+(-1).*d2.*d4).^(-2).*(d2.*(d4.^2+(-1).*d3.*d5).*Dd4+((-1).* ...
            d3.^2+d2.*d4).*((-2).*d4.*Dd4+d3.*Dd5));
        d2D1 = (d3.^2+(-1).*d2.*d4).^(-3).*((-1).*(d3.^2+(-1).*d2.*d4).*(d2.* ...
            D2d5.*((-1).*d3.^2+d2.*d4)+D2d4.*(d3.^3+(-1).*d2.^2.*d5))+2.*((-1) ...
            .*d2.*d3.^3+d2.^3.*d5).*Dd4.^2+2.*d2.^2.*(d3.^2+(-1).*d2.*d4).* ...
            Dd4.*Dd5);
        d2D2 = (d3.^2+(-1).*d2.*d4).^(-3).*((-1).*(d3.^2+(-1).*d2.*d4).*(D2d5.* ...
            d3.^3+(-1).*d3.*(d2.*D2d5+2.*D2d4.*d3).*d4+d2.*D2d4.*d4.^2+d2.* ...
            D2d4.*d3.*d5)+2.*(d3.^4+(-1).*d2.^2.*d3.*d5).*Dd4.^2+2.*d2.*d3.*(( ...
            -1).*d3.^2+d2.*d4).*Dd4.*Dd5);
        
        Num = 1 + N1.*u + N2.*u2 + N3.*u3;
        Den = 1 + D1.*u + D2.*u2;
        
        dNum =  N1 +u.*dN1 + 2*N2.*u + dN2.*u2 + 3*N3.*u2 + dN3.*u3;
        dDen =  D1 +u.*dD1 + 2*D2.*u + dD2.*u2;
        d2Num = 2*dN1 +u.*d2N1 + 2*N2  + 4*dN2.*u + d2N2.*u2 + 6*N3.*u + 6*dN3.*u2 + d2N3.*u3;
        d2Den = 2*dD1 +u.*d2D1 + 2*D2  + 4*dD2.*u + d2D2.*u2;
        
        % Finally, the D function and its derivative
        D  =  Num./Den;
        dD = -u2.*(dNum.*Den - Num.*dDen)./(Den.^2);
        d2D = -2*u.*dD + u4.*( (d2Num.*Den-Num.*d2Den)./(Den.^2)...
            -2.*dDen.*(dNum.*Den-Num.*dDen)./(Den.^3));
        
        
    otherwise
        
        Dp   = 1.0 + 6*nu*u2 - 2*(3.0*nu-26.0)*nu*u3; % Pade' resummation of D
        dDp  = -12*nu*u3 + 6*(3.0.*nu-26)*nu*u4;
        d2Dp = -u2.*(-36*nu*u2 + 24*(3*nu-26)*nu*u3);
        D    = 1./Dp;
        dD   = 6*u2.*(2*nu*u-(3*nu-26)*nu*u2).*D.^2;
        d2D  = 2*dDp.^2./Dp.^3 - d2Dp./Dp.^2;
        %B   = D./A;
        %dB  = (dD.*A - D.*dA)./A.^2;
        
end


end