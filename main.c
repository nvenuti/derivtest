#include "header.h"
#include <gsl/gsl_math.h>

/* 
RUN WITH ARGUMENT 'a', 'd', 'q', or 'all'
*/


int main(int argc, char *argv[])
{

  if (argc != 2)
  {
    printf("run with argument to specify potential to test\n");
    printf("a, d or q or all\n");
    exit(EXIT_FAILURE);
  }

  // common variables to all cases

  double nu = 0.2;

  int rvals       = 10000;
  // double rmax      = 4.;
  double rmin      = 2.;
  double dr        = 1e-3;

  double *r_array   = malloc(sizeof(double) * rvals);
  double *u_array   = malloc(sizeof(double) * rvals);

  // Filler array to put in output of function writegood when we
  // want to use only the values of one funciton instead of relative
  // difference of two
  double *filler_array = malloc(sizeof(double) * rvals);

  
  for (int i=0; i < rvals; i++)
  {
    r_array[i] = dr * i +rmin;
    u_array[i] = 1/r_array[i];
    filler_array[i]     = 0;
  }


  // derivatives of A potential
  if (!strcmp(argv[1], "a") || !strcmp(argv[1], "all"))
  {
    work_with_a(nu, rvals, rmin, dr, r_array, u_array, filler_array);
  }
    
  // derivatives of D potential
  if (!strcmp(argv[1], "d") || !strcmp(argv[1], "all"))
  {
    work_with_d(nu, rvals, rmin, dr, r_array, u_array, filler_array);
  }

  // derivatives of Q potential
  if (!strcmp(argv[1], "q") || !strcmp(argv[1], "all"))
  {
    work_with_q(nu, rvals, rmin, dr, r_array, u_array, filler_array);
  }
  else
  {
    printf("%s not among the options\n", argv[1]);
    printf("run with argument to specify potential to test\n");
    printf("a, d or q or all\n");
    exit(EXIT_FAILURE);
  }

  free(filler_array);
  free(r_array);  
  free(u_array);

}


