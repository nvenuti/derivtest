#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <signal.h>

#include <gsl/gsl_math.h>



#define Pi (3.1415926535897932384626433832795028)
#define EulerGamma (0.5772156649015328606065121)
#define SQ(a) (a*a)

# define getName(var, str)  sprintf(str, "%s", #var) 


//------------------------------------------------------------------------------
//function declarations

void eob_metric_A5PNlog(double r, double nu, double *A, double *dA, double *d2A);
void eob_metric_A5PNlogP33(double r, double nu, double *A, double *dA, double *d2A);
void eob_metric_D5PNP32(double r, double nu, double *A, double *dA, double *d2A);
void eob_metric_D3PN(double r, double nu, double *D, double *dD, double *d2D);
void eob_metric_Q3PN(double r, double prstar, double nu, double *Q, double *dQ_du, double *dQ_dprstar, 
                     double *d2Q_du2, double *ddQ_drdprstar, double *d2Q_dprstar2,
                     double *d3Q_dr2dprstar, double *d3Q_drdprstar2, double *d3Q_dprstar3);
void eob_metric_Q5PNloc(double r, double prstar, double nu, double *Q, double *dQ_du, double *dQ_dprstar, 
                     double *d2Q_du2, double *d2Q_drdprstar, double *d2Q_dprstar2,
                     double *d3Q_dr2dprstar, double *d3Q_drdprstar2, double *d3Q_dprstar3);

void work_with_a(double nu, int rvals, double rmin, double dr, double *r_array, double *u_array, double *filler_array);
void work_with_d(double nu, int rvals, double rmin, double dr, double *r_array, double *u_array, double *filler_array);
void work_with_q(double nu, int rvals, double rmin, double dr, double *r_array, double *u_array, double *filler_array);


int D0(double *f, double dx, int n, double *df);
int D2(double *f, double dx, int n, double *d2f);
int D0_x_4(double *f, double *x, int n, double *df);
double d4(double *x, double *y, int i);
double l_deriv(double *x,int i,int j);
void writegood(char *instruction,
                char *name, double *numerical, double *analytical,
                double *x_array, double dx, int array_len);

//------------------------------------------------------------------------------

