import numpy as np
import matplotlib.pyplot as plt
import sys


# script to make all comparison plots we might be interested in
# follows naming conventions of the files produced by dtest.c

def make_plot(txt_file_name):
    relativepath = os.path.join('outdir', txt_file_name)
    a = np.loadtxt(relativepath, delimiter=',')

    fig1, ax= plt.subplots(figsize=(10, 5))
    fig1.suptitle(txt_file_name[:len(txt_file_name)-4])

    ax.grid()

    if len(sys.argv) > 2 and sys.argv[2] == 'loglog':
        ax.plot(a[:, 0], abs(a[:, 1]))
        ax.set_yscale('log')
        ax.set_xscale('log')
    else:
        ax.plot(a[:, 0], a[:, 1])

    if txt_file_name[:8] == 'an_num_p' or txt_file_name[:9] == 'new_old_p' or txt_file_name[0] == 'p':
        ax.set_xlabel('prstar')
    else:
        ax.set_xlabel('r')

    fig1.tight_layout()
    fig1.show()

    


if len(sys.argv) == 1:
    print('run with argument to specify potential')
    print('\'a\', \'d\' or \'q\'')
    sys.exit()

print('   \'an_num\' are differences divided by the error estimate of the numerical derivative')
print('    <first_term>_<second_term> = (first_term - second_term)/ (order of numerical error)\n')
print('    \'new_old\' are relative differences')
print('    <first_term>_<second_term> = (first_term - second_term)/ (first_term)')

# derivatives of A potential
if sys.argv[1] == 'a':

    #------------------------------------------------------------------------------
    # numerical-analytical derivatives in r

    make_plot('an_num_u_dA.txt')

    make_plot('an_num_u_d2A.txt')

    make_plot('A.txt')

    make_plot('Aold.txt')


    #------------------------------------------------------------------------------
    # new potential-old potential (analytical)

    make_plot('new_old_dA.txt')

    make_plot('new_old_d2A.txt')

    make_plot('new_old_A.txt')


# derivatives  of D potential
if sys.argv[1] == 'd':
    
    #------------------------------------------------------------------------------
    # numerical-analytical derivatives in r

    make_plot('an_num_u_dD.txt')

    make_plot('an_num_u_d2D.txt')

    make_plot('D.txt')

    make_plot('Dold.txt')


    #------------------------------------------------------------------------------
    # new potential-old potential (analytical)

    make_plot('new_old_dD.txt')

    make_plot('new_old_d2D.txt')

    make_plot('new_old_D.txt')


# derivatives  of Q potential
if sys.argv[1] == 'q':
    #------------------------------------------------------------------------------
    # numerical-analytical derivatives in u

    make_plot('an_num_u_dQ_du.txt')

    make_plot('an_num_u_d2Q_du2.txt')

    #------------------------------------------------------------------------------
    # numerical-analytical derivatives in r

    make_plot('an_num_r_d2Q_drdprstar.txt')

    make_plot('an_num_r_d3Q_dr2dprstar.txt')

    make_plot('an_num_r_d3Q_drdprstar2.txt')

    #------------------------------------------------------------------------------
    # numerical-analytical derivatives in p


    make_plot('an_num_p_dQ_dprstar.txt')

    make_plot('an_num_p_d2Q_dprstar2.txt')

    make_plot('an_num_p_d3Q_dprstar3.txt')

    make_plot('an_num_p_d3Q_drdprstar2.txt')

    make_plot('Q.txt')

    make_plot('Qold.txt')

    make_plot('new_old_Q.txt')

    make_plot('p_Q.txt')

    make_plot('p_Qold.txt')

    make_plot('new_old_p_Q.txt')



# wait with plots open
input('Close figures and enter when done...')




