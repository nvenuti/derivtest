#include "header.h"
#include <gsl/gsl_math.h>




//-----------------------------------------------------------------------------
// OLD AND NEW POTENTIAL FUNCTIONS THAT WE WANT TO TEST

//-----------------------------------------------------------------------------



void eob_metric_A5PNlog(double r, double nu, double *A, double *dA, double *d2A)
{

  /* shortcuts */
  double nu2 = nu*nu;
  double pi2 = Pi*Pi;
  double pi4 = pi2*pi2;
  double u    = 1./r;
  double u2   = u*u;
  double u3   = u*u2;
  double u4   = u2*u2;
  double u5   = u4*u;
  double u6   = u5*u;
  double u7   = u6*u;
  double u10  = u5*u5;
  double u8   = u5*u3;
  double u9   = u8*u;
  double logu = log(u);

  double a5c0 = -4237./60. + 2275./512.*pi2 + 256./5.*log(2) + 128./5.*EulerGamma;
  double a5c1 = -221./6.   + 41./32.*pi2;
  double a5   =  a5c0 + nu*a5c1;
  double a6   =  0;
  
  /* 4PN and 5PN coefficients including all known log terms */
  double a5tot  = a5  + 64./5.*logu;
  double a6tot  = a6  + (-7004./105. - 144./5.*nu)*logu;
  double a5tot2 = a5tot*a5tot;
  
  /* Coefficients of the Padeed function */
  double N1 = (-3*(-512 - 32*nu2 + nu*(3520 + 32*a5tot + 8*a6tot - 123*pi2)))/(-768 + nu*(3584 + 24*a5tot - 123*pi2));
  double D1 = (nu*(-3392 - 48*a5tot - 24*a6tot + 96*nu + 123*pi2))/(-768 + nu*(3584 + 24*a5tot - 123*pi2));
  double D2 = (2*nu*(-3392 - 48*a5tot - 24*a6tot + 96*nu + 123*pi2))/(-768 + nu*(3584 + 24*a5tot - 123*pi2));
  double D3 = (-2*nu*(6016 + 48*a6tot + 3392*nu + 24*a5tot*(4 + nu) - 246*pi2 - 123*nu*pi2))/(-768 + nu*(3584 + 24*a5tot - 123*pi2));
  double D4 = -(nu*(-4608*a6tot*(-4 + nu) + a5tot*(36864 + nu*(72192 - 2952*pi2)) + nu*(2048*(5582 + 9*nu) - 834432*pi2 + 15129*pi4)))/(96.*(-768 + nu*(3584 + 24*a5tot - 123*pi2)));
  double D5 = (nu*(-24*a6tot*(1536 + nu*(-3776 + 123*pi2)) + nu*(-2304*a5tot2 + 96*a5tot*(-3392 + 123*pi2) - (-3776 + 123*pi2)*(-3008 - 96*nu + 123*pi2))))/(96.*(-768 + nu*(3584 + 24*a5tot - 123*pi2)));
  
  /* First derivatives */
  double dN1 = (160*nu*(-828672 - 32256*nu2 + 756*nu*(-768 + nu*(3584 + 24*a5 - 123*pi2)) + nu*(5006848 + 42024*a5 + 8064*a6 - 174045*pi2)))/(7.*gsl_pow_int(1536*logu*nu + 5*(-768 + nu*(3584 + 24*a5 - 123*pi2)),2)*u);
  double dD1 = (160*nu*(-828672 - 32256*nu2 + 756*nu*(-768 + nu*(3584 + 24*a5 - 123*pi2)) + nu*(5006848 + 42024*a5 + 8064*a6 - 174045*pi2)))/(7.*gsl_pow_int(1536*logu*nu + 5*(-768 + nu*(3584 + 24*a5 - 123*pi2)),2)*u);
  double dD2 = (320*nu*(-828672 - 32256*nu2 + 756*nu*(-768 + nu*(3584 + 24*a5 - 123*pi2)) + nu*(5006848 + 42024*a5 + 8064*a6 - 174045*pi2)))/(7.*gsl_pow_int(1536*logu*nu + 5*(-768 + nu*(3584 + 24*a5 - 123*pi2)),2)*u);
  double dD3 = (640*nu*(-828672 - 32256*nu2 + 756*nu*(-768 + nu*(3584 + 24*a5 - 123*pi2)) + nu*(5006848 + 42024*a5 + 8064*a6 - 174045*pi2)))/(7.*gsl_pow_int(1536*logu*nu + 5*(-768 + nu*(3584 + 24*a5 - 123*pi2)),2)*u);
  double dD4 = (-320*(-4 + nu)*nu*(-828672 - 32256*nu2 + 756*nu*(-768 + nu*(3584 + 24*a5 - 123*pi2)) + nu*(5006848 + 42024*a5 + 8064*a6 - 174045*pi2)))/(7.*gsl_pow_int(1536*logu*nu + 5*(-768 + nu*(3584 + 24*a5 - 123*pi2)),2)*u);
  double dD5 = (nu*(-8400*nu*(-24*(a6 - (4*logu*(1751 + 756*nu))/105.)*(1536 + nu*(-3776 + 123*pi2)) + nu*(-2304*gsl_pow_int(a5 + (64*logu)/5.,2) + 96*(a5 + (64*logu)/5.)*(-3392 + 123*pi2) - (-3776 + 123*pi2)*(-32*(94 + 3*nu) + 123*pi2))) - (1536*logu*nu + 5*(-768 + nu*(3584 + 24*a5 - 123*pi2)))*(4128768*logu*nu + 5*(-2689536 + nu*(11170624 + 64512*a5 - 380685*pi2) - 756*nu*(1536 + nu*(-3776 + 123*pi2))))))/(2625.*gsl_pow_int(-768 + nu*(3584 + 24*(a5 + (64*logu)/5.) - 123*pi2),2)*u);
  
  /* Numerator and denominator of the Pade */
  double Num = 1 + N1*u;
  double Den = 1 + D1*u + D2*u2 + D3*u3 + D4*u4 + D5*u5;
  *A = Num/Den;
    
  /* First derivative */
  double dNum  = dN1*u + N1;
  double dDen  = D1 + u*(dD1 + 2*D2) + u2*(dD2 + 3*D3) + u3*(dD3 + 4*D4) + u4*(dD4 + 5*D5) + dD5*u5;
  
  /* Derivative of A function with respect to u */
  double prefactor = (*A)/(Num*Den);
  double dA_u      = prefactor*(dNum*Den - dDen*Num);

  /* Derivative of A with respect to r */
  /* *dA = -u2*dA_u; */

  *dA = dA_u;

  if (d2A != NULL) {
    
    /* Second derivatives of Pade coefficients */
    double d2N1 = (160*nu*(-3840 + 1536*logu*nu + nu*(20992 + 120*a5 - 615*pi2))*(828672 + nu*(-42024*a5 - 8064*a6 + 3584*(-1397 + 9*nu) + 174045*pi2) + 756*nu*(768 + nu*(-3584 - 24*a5 + 123*pi2))))/(7.*gsl_pow_int(1536*logu*nu + 5*(-768 + nu*(3584 + 24*a5 - 123*pi2)),3)*u2);
    double d2D1 = (160*nu*(-3840 + 1536*logu*nu + nu*(20992 + 120*a5 - 615*pi2))*(828672 + nu*(-42024*a5 - 8064*a6 + 3584*(-1397 + 9*nu) + 174045*pi2) + 756*nu*(768 + nu*(-3584 - 24*a5 + 123*pi2))))/(7.*gsl_pow_int(1536*logu*nu + 5*(-768 + nu*(3584 + 24*a5 - 123*pi2)),3)*u2);
    double d2D2 = (320*nu*(-3840 + 1536*logu*nu + nu*(20992 + 120*a5 - 615*pi2))*(828672 + nu*(-42024*a5 - 8064*a6 + 3584*(-1397 + 9*nu) + 174045*pi2) + 756*nu*(768 + nu*(-3584 - 24*a5 + 123*pi2))))/(7.*gsl_pow_int(1536*logu*nu + 5*(-768 + nu*(3584 + 24*a5 - 123*pi2)),3)*u2);
    double d2D3 = (640*nu*(-3840 + 1536*logu*nu + nu*(20992 + 120*a5 - 615*pi2))*(828672 + nu*(-42024*a5 - 8064*a6 + 3584*(-1397 + 9*nu) + 174045*pi2) + 756*nu*(768 + nu*(-3584 - 24*a5 + 123*pi2))))/(7.*gsl_pow_int(1536*logu*nu + 5*(-768 + nu*(3584 + 24*a5 - 123*pi2)),3)*u2);
    double d2D4 = (320*(-4 + nu)*nu*(-828672 + 756*nu*(-768 + nu*(3584 + 24*a5 - 123*pi2)) + nu*(5006848 + 42024*a5 + 8064*a6 - 32256*nu - 174045*pi2))*(-3840 + 1536*logu*nu + nu*(20992 + 120*a5 - 615*pi2)))/(7.*gsl_pow_int(1536*logu*nu + 5*(-768 + nu*(3584 + 24*a5 - 123*pi2)),3)*u2);
    double d2D5 = (nu*(gsl_pow_int(1536*logu*nu + 5*(-768 + nu*(3584 + 24*a5 - 123*pi2)),2)*(4128768*logu*nu - 7680*(1751 + 756*nu) + nu*(64*(808193 + 5040*a5 + 223020*nu) - 615*(3095 + 756*nu)*pi2)) + 3072*nu*(1536*logu*nu + 5*(-768 + nu*(3584 + 24*a5 - 123*pi2)))*(4128768*logu*nu - 7680*(1751 + 756*nu) + 5*nu*(64*(174541 + 1008*a5 + 44604*nu) - 123*(3095 + 756*nu)*pi2)) + 25804800*nu2*(-24*(a6 - (4*logu*(1751 + 756*nu))/105.)*(1536 + nu*(-3776 + 123*pi2)) + nu*(-2304*gsl_pow_int(a5 + (64*logu)/5.,2) + 96*(a5 + (64*logu)/5.)*(-3392 + 123*pi2) - (-3776 + 123*pi2)*(-32*(94 + 3*nu) + 123*pi2))) + 42000*nu*(-768 + nu*(3584 + 24*(a5 + (64*logu)/5.) - 123*pi2))*(-24*(a6 - (4*logu*(1751 + 756*nu))/105.)*(1536 + nu*(-3776 + 123*pi2)) + nu*(-2304*gsl_pow_int(a5 + (64*logu)/5.,2) + 96*(a5 + (64*logu)/5.)*(-3392 + 123*pi2) - (-3776 + 123*pi2)*(-32*(94 + 3*nu) + 123*pi2)))))/(13125.*gsl_pow_int(-768 + nu*(3584 + 24*(a5 + (64*logu)/5.) - 123*pi2),3)*u2);
    
    /* Second derivative of numerator and denominator */
    double d2Num = 2.*dN1 + d2N1*u;
    double d2Den = 2.*(D2 + dD1) + u*(6.*D3 + 4.*dD2 + d2D1) + u2*(12.*D4 + 6.*dD3 + d2D2) + u3*(20.*D5 + 8.*dD4 + d2D3) + u4*(10.*dD5 + d2D4) + u5*d2D5;
    
    /* Second derivative with respect of u */
    double d2A_u = prefactor*(2.*dDen*dDen*(*A) - 2.*dNum*dDen + Den*d2Num - d2Den*Num);

    *d2A = d2A_u;
    
    /* Second derivative with respect of r */
    /* *d2A = u4*d2A_u + 2.*u3*dA_u; */
    
  }
}


/*
  EOB Metric A function 5PN log resummed with (3, 3) Pade 
  (Pade coefficients already computed in matlab)
*/
void eob_metric_A5PNlogP33(double r, double nu, double *A, double *dA, double *d2A)
{

  /* shortcuts */
  double nu2 = nu*nu;
  double pi2 = Pi*Pi;
  double pi4 = pi2*pi2;
  double pi6 = pi4*pi2;
  double ln2  = log(2);
  double ln3  = log(3);

  double a5c0       = -4237/60 + 2275/512*pi2 + 256/5*log(2) + 128/5*EulerGamma;
  double a5c1       = -221/6   + 41/32*pi2;
  double a5         =  a5c0 + nu*a5c1;
  double a6         =  0;

  double u       = 1./r;
  double u2      = u*u;
  double u3      = u*u2;
  double u4      = u2*u2;
  double logu    = log(u);

  double a5tot = a5  + 64/5*logu;
  double a6tot = a6  + (-7004/105 - 144/5*nu)*logu;



  /*----------------------------------------------------------
   preliminary: coefficients of the Pade and its derivatives
  ----------------------------------------------------------*/
  double N0     = -1737228288  + 3538944*a5tot + 7077888*nu + 142073856*pi2 - 2904768*pi4;
  double N1     = -27216576512 - 3538944*a6tot + 3338735616*pi2 - 136524096*pi4 + 1860867*pi6 + a5tot*(110886912 - 4534272*pi2);
  double D0     =  N0;
  double D1     = -18432*(192*a6tot + 6016*nu - 246*nu*pi2 + a5tot*(-3008 + 123*pi2));
  double D2     = -192*(9216*a5tot*a5tot + 18432*a5tot*nu + (-3008 + 123*pi2)*(96*a6tot + nu*(3008 - 123*pi2)));
  double D3     =  nu*(-3538944*a6tot - 36864*a5tot*(-3008 + 123*pi2) + pow(-3008 + 123*pi2,3));

  /*1st derivatives of the above coefficients entering danu/du*/
  double dN0 = 226492416./(5*u);
  double dN1 = (-7077888*(-8186 - 504*nu + 287*pi2))/(35.*u);
  double dD0 = dN0;
  double dD1 = (-1179648*(-4*(7015 + 756*nu) + 861*pi2))/(35.*u);
  double dD2 = (-24576*(4128768*logu + 5*(64512*a5 + 64*(82297 + 36540*nu) - 123*(1751 + 756*nu)*pi2)))/(175.*u);
  double dD3 = (-7077888*nu*(-8186 - 504*nu + 287*pi2))/(35.*u);

  /* 2nd derivatives of the coefficients */
  double d2N0 = -226492416./(5.*u2);
  double d2N1 = -(-7077888*(-8186 - 504*nu + 287*pi2))/(35.*u2);
  double d2D0 = d2N0;
  double d2D1 = -(-1179648*(-4*(7015 + 756*nu) + 861*pi2))/(35.*u2);
  double d2D2 = -(-24576*(22206272 + 322560*a5 + 4128768*logu + 11692800*nu - 1076865*pi2 - 464940*nu*pi2))/(175.*u2);
  double d2D3 = -(-7077888*nu*(-8186 - 504*nu + 287*pi2))/(35.*u2);

  /* the small a(u,nu) function*/
  double Num  = N0 + N1*u;
  double Den  = D0 + D1*u + D2*u2 + D3*u3;
  double anu  = Num/Den;

  /* derivative of anu with respect to u*/
  double dNum    = dN0 + N1 + u*dN1;
  double dDen    = dD0 + D1 + u*dD1 + 2.*u*D2 + u2*dD2 + 3*u2*D3 + u3*dD3;
  double danu_du = anu*(dNum/Num - dDen/Den);

  /* second derivative of anu with respect to u*/
  double d2Num = d2N0 + 2*dN1 + u*d2N1;
  double d2Den = u3*d2D3 + u2*(d2D2+6*dD3) + u*(d2D1 + 4*dD2 + 6*D3) + d2D0 + 2*dD1 + 2*D2;
  double d2anu_du2 = anu/(Num*Den)*(2.*dDen*dDen*anu - 2.*dNum*dDen + Den*d2Num - d2Den*Num);

  /* derivatives of A with respect to u */
  double dA_du  = -2 + 6*nu*u2*anu + 2*nu*u3*danu_du;
  double d2A_du = 12*nu*u*anu + 12*nu*u2*danu_du + 2*nu*u3*d2anu_du2;

  /* the A function and its derivatives with respect to r */
  /*
  *dA  =  -u2*dA_du;
  *d2A =   u4*d2A_du + 2*u3*dA_du;
  */

  /* the A function and its derivatives with respect to r */
  
  *A   =  1-2*u + 2*nu*u3*anu;
  *dA  =  dA_du;
  *d2A =  d2A_du;
}




/** EOB Metric D function at 3PN, resummed */

void eob_metric_D3PN(double r, double nu, double *D, double *dD, double *d2D)
{

  /* shortcuts */
  double u  = 1./r;
  double u2 = u*u;
  double u3 = u2*u;

  double Dp       = 1.0 + 6.*nu*u2 - 2.*(3.*nu-26.)*nu*u3; // Pade' resummation of D
  double dDp_du   = 6.*nu*u*(2. - (3.*nu-26.)*u);
  double d2Dp_du2 = 12.*nu*(1. - (3.*nu-26.)*u);
  double D_tmp    = 1./Dp;
  
  /* derivatives wrt to u */
  *D   = D_tmp;
  *dD  = -SQ(D_tmp)*dDp_du;
  *d2D = 2.*SQ(D_tmp)*D_tmp*SQ(dDp_du) - SQ(D_tmp)*d2Dp_du2;

}




/*
  EOB metric D potential rewritten with (3, 2) Pade 
  (Pade coefficients already computed in matlab)
*/
void eob_metric_D5PNP32(double r, double nu, double *D, double *dD, double *d2D)
{

  /* shortcuts */
  double nu2 = nu*nu;
  double pi2 = Pi*Pi;
  double pi4 = pi2*pi2;
  double pi6 = pi4*pi2;
  double ln2  = log(2);
  double ln3  = log(3);

  double u       = 1./r;
  double u2      = u*u;
  double u3      = u*u2;
  double u4      = u2*u2;
  double logu    = log(u);


  // only analytically uncalculated 5PN coefficient set to zero 
  double d5nu2 = 0.;        
  double d2    = -6*nu;
  double d3    = -52*nu + 6*nu2;

  double c1    = 533/45 - 1184/15*EulerGamma + 23761/1536*pi2  + 6496/15*ln2 - 2916/5*ln3;
  double c2    = 296 - 123/16*pi2;
  double d4c   = c1 + nu*c2;
  double d4log = -592/15;
  double d4    = nu*(d4c + d4log*log(u));
  // its derivative
  double Dd4   = nu*d4log/u;
  double D2d4  = -nu*d4log/u2;

  double d5c   = (-294464/175) + (2840/7)*EulerGamma + (-120648/35)*ln2 + (19683/7)* \
          ln3 + ((-2216/105) + (-1)*d5nu2 + (6784/15)*EulerGamma + (326656/21)* \
          ln2 + (-58320/7)*ln3)*nu + (63707/512)*pi2 + nu2*((-1285/3) + (205/16) \
          *pi2);
  double d5log = (1420/7) + (3392/15)*nu;
  double d5    = nu*(d5c  +  d5log*log(u));
  double Dd5   = nu*d5log/u;
  double D2d5  = -nu*d5log/u2;
  
  double d2_2 = d2*d2;
  double d2_3 = d2_2*d2;
  double d3_2 = d3*d3;
  double d3_3 = d3_2*d3;
  double d3_4 = d3_2*d3_2;
  double d4_2 = d4*d4;
  double Dd4_2 = Dd4*Dd4;

  // we define here this variable to then comfortably 
  // write powers of it
  double factor = 1/(d3_2 + (-1)*d2*d4);
  double factor2 = factor*factor;
  double factor3 = factor2*factor;

  double N1   = factor*((-1)*d3*d4 + d2*d5);
  double N2   = -factor*((-1)*d2*d3_2 + d2_2*d4 + (-1)*d4_2 + d3*d5);
  double N3   = factor*(d3_3 + (-2)*d2*d3*d4 + d2_2*d5);

  double dN1  = factor2*(((-1)*d3_3 + d2_2*d5)*Dd4 + d2*( \
      d3_2 + (-1)*d2*d4)*Dd5);
  double dN2  = factor2*((-1)*d2*d4_2*Dd4 + d3*d4*(2.*d3* \
      Dd4 + d2*Dd5) + (-1)*d3*(d2*d5*Dd4 + d3_2*Dd5));
  double dN3  = d2*factor2*(((-1)*d3_3 + d2_2*d5)*Dd4 + d2* \
      (d3_2 + (-1)*d2*d4)*Dd5);

  double d2N1 = factor3*((-1)*(d3_2 + (-1)*d2*d4)*(d2* \
      D2d5*((-1)*d3_2 + d2*d4) + D2d4*(d3_3 + (-1)*d2_2*d5)) + 2.*((-1) \
      *d2*d3_3 + d2_3*d5)*Dd4_2 + 2.*d2_2*(d3_2 + (-1)*d2*d4)* \
      Dd4*Dd5);
  double d2N2 = factor3*((-1)*(d3_2 + (-1)*d2*d4)*(D2d5* \
      d3_3 + (-1)*d3*(d2*D2d5 + 2.*D2d4*d3)*d4 + d2*D2d4*d4_2 + d2* \
      D2d4*d3*d5) + 2.*(d3_4 + (-1)*d2_2*d3*d5)*Dd4_2 + 2.*d2*d3*(( \
      -1)*d3_2 + d2*d4)*Dd4*Dd5);
  double d2N3 = d2*factor3*((-1)*(d3_2 + (-1)*d2*d4)*(d2* \
      D2d5*((-1)*d3_2 + d2*d4) + D2d4*(d3_3 + (-1)*d2_2*d5)) + 2.*((-1) \
      *d2*d3_3 + d2_3*d5)*Dd4_2 + 2.*d2_2*(d3_2 + (-1)*d2*d4)* \
      Dd4*Dd5);

  double D1   = factor*((-1)*d3*d4 + d2*d5);
  double D2   = -factor*((-1)*d4_2 + d3*d5);

  double dD1  = factor2*(((-1)*d3_3 + d2_2*d5)*Dd4 + d2*( \
      d3_2 + (-1)*d2*d4)*Dd5);
  double dD2  = factor2*(d2*(d4_2 + (-1)*d3*d5)*Dd4 + ((-1)* \
      d3_2 + d2*d4)*((-2)*d4*Dd4 + d3*Dd5));

  double d2D1 = factor3*((-1)*(d3_2 + (-1)*d2*d4)*(d2* \
      D2d5*((-1)*d3_2 + d2*d4) + D2d4*(d3_3 + (-1)*d2_2*d5)) + 2.*((-1) \
      *d2*d3_3 + d2_3*d5)*Dd4_2 + 2.*d2_2*(d3_2 + (-1)*d2*d4)* \
      Dd4*Dd5);
  double d2D2 = factor3*((-1)*(d3_2 + (-1)*d2*d4)*(D2d5* \
      d3_3 + (-1)*d3*(d2*D2d5 + 2.*D2d4*d3)*d4 + d2*D2d4*d4_2 + d2* \
      D2d4*d3*d5) + 2.*(d3_4 + (-1)*d2_2*d3*d5)*Dd4_2 + 2.*d2*d3*(( \
      -1)*d3_2 + d2*d4)*Dd4*Dd5);
  
  double Num = 1 + N1*u + N2*u2 + N3*u3;
  double Den = 1 + D1*u + D2*u2;
  
  double dNum =  N1 +u*dN1 + 2*N2*u + dN2*u2 + 3*N3*u2 + dN3*u3;
  double dDen =  D1 +u*dD1 + 2*D2*u + dD2*u2;
  double d2Num = 2*dN1 +u*d2N1 + 2*N2  + 4*dN2*u + d2N2*u2 + 6*N3*u + 6*dN3*u2 + d2N3*u3;
  double d2Den = 2*dD1 +u*d2D1 + 2*D2  + 4*dD2*u + d2D2*u2;
  
  // Finally, the D function and its derivative in r
  // *dD = -u2*(dNum*Den - Num*dDen)/(Den*Den);
  // *d2D = -2*u*dD + u4*( (d2Num*Den-Num*d2Den)/(Den*Den)\
  //     -2.*dDen*(dNum*Den-Num*dDen)/(Den*Den*Den));

  // Output the derivatives in u
  *D   =  Num/Den;
  *dD  = (dNum*Den - Num*dDen)/(Den*Den);
  *d2D = (d2Num*Den - Num*d2Den)/(Den*Den)\
      -2.*dDen*(dNum*Den - Num*dDen)/(Den*Den*Den);

}


/** EOB Metric Q function at 3PN */

void eob_metric_Q3PN(double r, double prstar, double nu, double *Q, double *dQ_du, double *dQ_dprstar, 
                     double *d2Q_du2, double *ddQ_drdprstar, double *d2Q_dprstar2,
                     double *d3Q_dr2dprstar, double *d3Q_drdprstar2, double *d3Q_dprstar3)
{
  const double z3 = 2.*nu*(4. - 3.*nu);
  double u  = 1./r;
  double u2 = u*u;
  double u3 = u2*u;
  double u4 = u3*u;
  double prstar2 = prstar*prstar;
  double prstar3 = prstar2*prstar;
  double prstar4 = prstar2*prstar2;

  *Q              =  z3*u2*prstar4;
  *dQ_du          =  2.*z3*u*prstar4;
  *dQ_dprstar     =  4.*z3*u2*prstar3;
  *d2Q_du2        =  2.*z3*prstar4;
  *ddQ_drdprstar  = -8.*z3*u3*prstar3;
  *d2Q_dprstar2   =  12.*z3*u2*prstar2;
  *d3Q_dr2dprstar =  24.*z3*u4*prstar3;
  *d3Q_drdprstar2 = -24.*z3*u3*prstar2;
  *d3Q_dprstar3   =  24.*z3*u2*prstar;

}



void eob_metric_Q5PNloc(double r, double prstar, double nu, double *Q, double *dQ_du, double *dQ_dprstar, 
                     double *d2Q_du2, double *d2Q_drdprstar, double *d2Q_dprstar2,
                     double *d3Q_dr2dprstar, double *d3Q_drdprstar2, double *d3Q_dprstar3)
{

  /* shortcuts */
  double pi2     = Pi*Pi;
  double nu2     = nu*nu;
  double nu3     = nu2*nu;
  double nu4     = nu3*nu;

  double prstar2 = prstar*prstar;
  double prstar3 = prstar2*prstar;
  double prstar4 = prstar2*prstar2;
  double prstar6 = prstar4*prstar2;
  double prstar8 = prstar4*prstar4;

  double u     = 1./r;
  double u2    = u*u;
  double u3    = u2*u;
  double u4    = u3*u;
  double du_dr = -u2;

  double z3  = 2.0*nu*(4.0-3.0*nu);
  double q42 = z3;


  double q62    = -2.783007636952232489*nu - 5.4*nu2 + 6.*nu3;
  double q43    = 92.711044284955949757*nu - 131.*nu2 + 10.*nu3;

  double q44loc = 453.70625272357658631*nu - 1081.8908931907688518*nu2 + \
                  602.31854041656388904*nu3;
  double q63loc = -21.096091643426789868*nu - 78.6*nu2 + 188.*nu3 - 14.*nu4;
  double q82loc = 6/7*nu + 18/7*nu2 + 24/7*nu3 - 6*nu4;

  double q82    = -1.5175121231629802709e6*nu + 3.3384202338272640486*nu2 + \
                  3.4285714285714285714*nu3 - 6.*nu4;
  double q63    = -217866.45869782848474*nu - 89.529832736260960467*nu2 + 188.*nu3 - 14.*nu4;
  double q44c   = 602.31854041656388904*nu3 + -1796.1366049802412531*nu2 + \
                  452.54216699669657073*nu;
  double q44log = 51.695238095238095238*nu - 118.4*nu2;
  double q44    = q44c + q44log*log(u);




  /* Q potential and all its derivatives */


  *Q = q42*u2*prstar4 + q43*u3*prstar4 + q62*u2*prstar6 \
      + q44loc*u4*prstar4 + q63loc*u3*prstar6 + q82loc*u2*prstar8;
  
  *dQ_du = 2.*q42*u*prstar4 + 3.*q43*u2*prstar4 + 2.*q62*u*prstar6 \
      + 4.*q44loc*u3*prstar4 + 3.*q63loc*u2*prstar6 + 2.*q82loc*u*prstar8;
  
  double dQ_dprstar2 = 2.*q42*u2*prstar2 + 2.*q43*u3*prstar2 + 3.*q62*u2*prstar4 \
              + 2.*q44loc*u4*prstar2 + 3.*q63loc*u3*prstar4 + 4.*q82loc*u2*prstar6;
  
  double d2Q_dudprstar2 = 4.*q42*u*prstar2 + 6.*q43*u2*prstar2 + 6.*q62*u*prstar4 \
              + 8.*q44loc*u3*prstar2 + 9.*q63loc*u2*prstar4 + 8.*q82loc*u*prstar6;   

  *d2Q_du2 = 2*q42 * prstar4 + 2* q62 * prstar6 + 6* q43 *prstar4 * u  \
            + 2* q82loc * prstar8 + 6* q63loc * prstar6 * u + 12 * q44loc * prstar4 * u2;               

  
  double d2Q_dprstar22 = 2.*q42*u2 + 2.*q43*u3 + 6.*q62*u2*prstar2 \
                + 2.*q44loc*u4 + 6.*q63loc*u3*prstar2 + 12.*q82loc*u2*prstar4; 

  
  double d3Q_dprstar23 = 6.*q62*u2+ 6.*q63loc*u3 + 24.*q82loc*u2*prstar2; 
  

  double d3Q_du2dprstar2 = 4* q42 * prstar2 + 6*q62*prstar4 + 12*q43*prstar2*u \
            + 8* q82loc *prstar6 + 18*q63loc*prstar4*u + 24 * q44loc * prstar2 * u2; 

  double d3Q_dudprstar22 = 4.*q42*u + 6.*q43*u2 + 12.*q62*u*prstar2 \
              + 8.*q44loc*u3 + 18.*q63loc*u2*prstar2 + 24.*q82loc*u*prstar4;  



  /* We also translate derivatives from prstar2 to prstar
     and some from u to r as needed for output*/


    *d2Q_drdprstar   = -2*prstar*u2*d2Q_dudprstar2;

    *d3Q_dr2dprstar  = 4*prstar*u3*d2Q_dudprstar2 + 2*u4*prstar*d3Q_du2dprstar2;

    *d3Q_drdprstar2  = -2*u2*d2Q_dudprstar2 - 4*prstar2*u2*d3Q_dudprstar22;

    *dQ_dprstar   = 2.*prstar*dQ_dprstar2;

    *d2Q_dprstar2 = 2.*dQ_dprstar2 + 4.*prstar2*d2Q_dprstar22;

    *d3Q_dprstar3 = 12.*prstar*d2Q_dprstar22 + 8.*prstar3*d3Q_dprstar23;

}
