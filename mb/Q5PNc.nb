(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     38286,        909]
NotebookOptionsPosition[     36763,        874]
NotebookOutlinePosition[     37207,        891]
CellTagsIndexPosition[     37164,        888]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell["Notebook to check the new 5PN expression of the Q potential", "Text",
 CellChangeTimes->{{3.894343553255916*^9, 3.8943435669906893`*^9}, {
  3.8943494426388817`*^9, 
  3.894349445190971*^9}},ExpressionUUID->"6083321c-3070-4b07-af27-\
0e9342481565"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"pi2", "=", 
   RowBox[{"Pi", "*", "Pi"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"nu2", "=", 
   RowBox[{"nu", "*", "nu"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"nu3", "=", 
   RowBox[{"nu2", "*", "nu"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"nu4", "=", 
    RowBox[{"nu3", "*", "nu"}]}], ";"}], "\n"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"prstar2", "=", 
   RowBox[{"prstar", "*", "prstar"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"prstar3", "=", 
   RowBox[{"prstar2", "*", "prstar"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"prstar4", "=", 
   RowBox[{"prstar2", "*", "prstar2"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"prstar6", "=", 
   RowBox[{"prstar4", "*", "prstar2"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"prstar8", "=", 
    RowBox[{"prstar4", "*", "prstar4"}]}], ";"}], "\n"}], "\n", 
 RowBox[{
  RowBox[{"u2", "=", 
   RowBox[{"u", "*", "u"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"u3", "=", 
   RowBox[{"u2", "*", "u"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"u4", "=", 
    RowBox[{"u3", "*", "u"}]}], ";"}], "\n"}], "\n", 
 RowBox[{
  RowBox[{"z3", "=", 
   RowBox[{"2.0", "*", "nu", "*", 
    RowBox[{"(", 
     RowBox[{"4.0", "-", 
      RowBox[{"3.0", "*", "nu"}]}], ")"}]}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"q42", "=", "z3"}], ";"}], "\n", 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"q62", "=", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "2.783007636952232489"}], "*", "nu"}], "-", 
    RowBox[{"5.4", "*", "nu2"}], "+", 
    RowBox[{"6.", "*", "nu3"}]}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"q43", "=", 
    RowBox[{
     RowBox[{"92.711044284955949757", "*", "nu"}], "-", 
     RowBox[{"131.", "*", "nu2"}], "+", 
     RowBox[{"10.", "*", "nu3"}]}]}], ";"}], "\[IndentingNewLine]"}], "\n", 
 RowBox[{"q44loc", "=", 
  RowBox[{
   RowBox[{"453.70625272357658631", "*", "nu"}], "-", 
   RowBox[{"1081.8908931907688518", "*", "nu2"}], "+", 
   RowBox[{"602.31854041656388904", "*", "nu3"}]}]}], "\n", 
 RowBox[{
  RowBox[{"q63loc", "=", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "21.096091643426789868"}], "*", "nu"}], "-", 
    RowBox[{"78.6", "*", "nu2"}], "+", 
    RowBox[{"188.", "*", "nu3"}], "-", 
    RowBox[{"14.", "*", "nu4"}]}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"q82loc", "=", 
    RowBox[{
     RowBox[{
      RowBox[{"6", "/", "7"}], "*", "nu"}], "+", 
     RowBox[{
      RowBox[{"18", "/", "7"}], "*", "nu2"}], "+", 
     RowBox[{
      RowBox[{"24", "/", "7"}], "*", "nu3"}], "-", 
     RowBox[{"6", "*", "nu4"}]}]}], ";"}], "\[IndentingNewLine]"}], "\n", 
 RowBox[{
  RowBox[{"q82", "=", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "1.5175121231629802709"}], "e6", "*", "nu"}], "+", 
    RowBox[{"3.3384202338272640486", "*", "nu2"}], "+", 
    RowBox[{"3.4285714285714285714", "*", "nu3"}], "-", 
    RowBox[{"6.", "*", "nu4"}]}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"q63", "=", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "217866.45869782848474"}], "*", "nu"}], "-", 
    RowBox[{"89.529832736260960467", "*", "nu2"}], "+", 
    RowBox[{"188.", "*", "nu3"}], "-", 
    RowBox[{"14.", "*", "nu4"}]}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"q44c", "=", 
   RowBox[{
    RowBox[{"602.31854041656388904", "*", "nu3"}], "+", 
    RowBox[{
     RowBox[{"-", "1796.1366049802412531"}], "*", "nu2"}], "+", 
    RowBox[{"452.54216699669657073", "*", "nu"}]}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"q44log", "=", 
   RowBox[{
    RowBox[{"51.695238095238095238", "*", "nu"}], "-", 
    RowBox[{"118.4", "*", "nu2"}]}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"q44", "=", 
   RowBox[{"q44c", "+", 
    RowBox[{"q44log", "*", 
     RowBox[{"Log", "[", "u", "]"}]}]}]}], ";"}], "\n"}], "Input",
 CellChangeTimes->{{3.8943435077105923`*^9, 3.894343541143084*^9}, {
   3.894343603622685*^9, 3.8943436221984777`*^9}, {3.894343803685018*^9, 
   3.894343834965186*^9}, {3.894343938486412*^9, 3.894343943492461*^9}, {
   3.8943440759965477`*^9, 3.894344079827483*^9}, {3.894344199395599*^9, 
   3.894344199826664*^9}, {3.894344464737076*^9, 3.8943444996571617`*^9}, 
   3.894345094228668*^9, {3.894348007896282*^9, 3.8943480219129143`*^9}, {
   3.894348080327598*^9, 3.894348151039844*^9}, {3.894348183152433*^9, 
   3.8943481989357843`*^9}, 3.8943492761694*^9, {3.894349308912015*^9, 
   3.894349339537128*^9}, {3.894349423431308*^9, 3.8943494373591957`*^9}, 
   3.894349870540269*^9},
 CellLabel->
  "In[140]:=",ExpressionUUID->"d961856c-f898-4cff-a192-b0ceba0db6ae"],

Cell[BoxData[
 RowBox[{
  RowBox[{"453.70625272357658631`19.656774764508768", " ", "nu"}], "-", 
  RowBox[{"1081.8908931907688518`19.034183465137993", " ", 
   SuperscriptBox["nu", "2"]}], "+", 
  RowBox[{"602.31854041656388904`19.77982623171894", " ", 
   SuperscriptBox["nu", "3"]}]}]], "Output",
 CellChangeTimes->{3.894349874005931*^9, 3.8943499694900637`*^9, 
  3.8943500449209633`*^9, 3.89435042765799*^9, 3.894350462479134*^9},
 CellLabel->
  "Out[156]=",ExpressionUUID->"08286be7-fa71-4fd8-bc19-04d0ea2d4a9f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"  ", 
  RowBox[{
   RowBox[{
    RowBox[{"Q", "[", 
     RowBox[{"prstar_", ",", " ", "u_"}], "]"}], "=", 
    RowBox[{
     RowBox[{"q42", "*", "u2", "*", "prstar4"}], "+", 
     RowBox[{"q43", "*", "u3", "*", "prstar4"}], "+", 
     RowBox[{"q62", "*", "u2", "*", "prstar6"}], "+", 
     RowBox[{"q44loc", "*", "u4", "*", "prstar4"}], "+", 
     RowBox[{"q63loc", "*", "u3", "*", "prstar6"}], "+", 
     RowBox[{"q82loc", "*", "u2", "*", "prstar8"}]}]}], "\n"}]}]], "Input",
 CellChangeTimes->{{3.894343594780572*^9, 3.894343594781097*^9}, {
   3.894343795367453*^9, 3.894343830805084*^9}, 3.894348039078614*^9, {
   3.89434808033885*^9, 3.894348080342072*^9}, {3.894348237424258*^9, 
   3.894348245383299*^9}, {3.8943492911806917`*^9, 3.894349304400094*^9}, 
   3.894349774220913*^9},
 CellLabel->
  "In[164]:=",ExpressionUUID->"5078db12-c6e8-4022-bb2a-85cff913bc57"],

Cell[BoxData[
 RowBox[{
  RowBox[{"2.`", " ", 
   RowBox[{"(", 
    RowBox[{"4.`", "\[VeryThinSpace]", "-", 
     RowBox[{"3.`", " ", "nu"}]}], ")"}], " ", "nu", " ", 
   SuperscriptBox["prstar", "4"], " ", 
   SuperscriptBox["u", "2"]}], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "2.783007636952232489`18.444514398098924"}], " ", "nu"}], 
     "-", 
     RowBox[{"5.4`", " ", 
      SuperscriptBox["nu", "2"]}], "+", 
     RowBox[{"6.`", " ", 
      SuperscriptBox["nu", "3"]}]}], ")"}], " ", 
   SuperscriptBox["prstar", "6"], " ", 
   SuperscriptBox["u", "2"]}], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     FractionBox[
      RowBox[{"6", " ", "nu"}], "7"], "+", 
     FractionBox[
      RowBox[{"18", " ", 
       SuperscriptBox["nu", "2"]}], "7"], "+", 
     FractionBox[
      RowBox[{"24", " ", 
       SuperscriptBox["nu", "3"]}], "7"], "-", 
     RowBox[{"6", " ", 
      SuperscriptBox["nu", "4"]}]}], ")"}], " ", 
   SuperscriptBox["prstar", "8"], " ", 
   SuperscriptBox["u", "2"]}], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"92.711044284955949757`19.96713147293964", " ", "nu"}], "-", 
     RowBox[{"131.`", " ", 
      SuperscriptBox["nu", "2"]}], "+", 
     RowBox[{"10.`", " ", 
      SuperscriptBox["nu", "3"]}]}], ")"}], " ", 
   SuperscriptBox["prstar", "4"], " ", 
   SuperscriptBox["u", "3"]}], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "21.096091643426789868`19.324202003405976"}], " ", "nu"}], 
     "-", 
     RowBox[{"78.6`", " ", 
      SuperscriptBox["nu", "2"]}], "+", 
     RowBox[{"188.`", " ", 
      SuperscriptBox["nu", "3"]}], "-", 
     RowBox[{"14.`", " ", 
      SuperscriptBox["nu", "4"]}]}], ")"}], " ", 
   SuperscriptBox["prstar", "6"], " ", 
   SuperscriptBox["u", "3"]}], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"453.70625272357658631`19.656774764508768", " ", "nu"}], "-", 
     RowBox[{"1081.8908931907688518`19.034183465137993", " ", 
      SuperscriptBox["nu", "2"]}], "+", 
     RowBox[{"602.31854041656388904`19.77982623171894", " ", 
      SuperscriptBox["nu", "3"]}]}], ")"}], " ", 
   SuperscriptBox["prstar", "4"], " ", 
   SuperscriptBox["u", "4"]}]}]], "Output",
 CellChangeTimes->{3.8943497749901752`*^9, 3.894349969515321*^9, 
  3.894350044948338*^9, 3.8943504276859818`*^9, 3.894350462510169*^9},
 CellLabel->
  "Out[164]=",ExpressionUUID->"a93c59fe-606f-4205-af7f-823bfd7ddb8f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{" ", 
  RowBox[{
   RowBox[{"Qmatlab", "[", 
    RowBox[{"prstar_", ",", " ", "u_"}], "]"}], "=", 
   RowBox[{
    RowBox[{"2.", "*", 
     RowBox[{"(", 
      RowBox[{"4.", "-", 
       RowBox[{"3.", "*", "nu"}]}], ")"}], "*", "nu", "*", 
     RowBox[{"prstar", "^", "4"}], "*", 
     RowBox[{"u", "^", "2"}]}], "+", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"-", "2.783007636952232489`18.444514398098924"}], "*", "nu"}],
        "-", 
       RowBox[{"5.4", "*", 
        RowBox[{"nu", "^", "2"}]}], "+", 
       RowBox[{"6.", "*", 
        RowBox[{"nu", "^", "3"}]}]}], ")"}], "*", 
     RowBox[{"prstar", "^", "6"}], "*", 
     RowBox[{"u", "^", "2"}]}], "+", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"6", "*", "nu"}], ")"}], "/", "7"}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"18", "*", 
          RowBox[{"nu", "^", "2"}]}], ")"}], "/", "7"}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"24", "*", 
          RowBox[{"nu", "^", "3"}]}], ")"}], "/", "7"}], "-", 
       RowBox[{"6", "*", 
        RowBox[{"nu", "^", "4"}]}]}], ")"}], "*", 
     RowBox[{"prstar", "^", "8"}], "*", 
     RowBox[{"u", "^", "2"}]}], "+", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"92.711044284955949757`19.96713147293964", "*", "nu"}], "-", 
       RowBox[{"131.", "*", 
        RowBox[{"nu", "^", "2"}]}], "+", 
       RowBox[{"10.", "*", 
        RowBox[{"nu", "^", "3"}]}]}], ")"}], "*", 
     RowBox[{"prstar", "^", "4"}], "*", 
     RowBox[{"u", "^", "3"}]}], "+", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"-", "21.096091643426789868`19.324202003405976"}], "*", 
        "nu"}], "-", 
       RowBox[{"78.6", "*", 
        RowBox[{"nu", "^", "2"}]}], "+", 
       RowBox[{"188.", "*", 
        RowBox[{"nu", "^", "3"}]}], "-", 
       RowBox[{"14.", "*", 
        RowBox[{"nu", "^", "4"}]}]}], ")"}], "*", 
     RowBox[{"prstar", "^", "6"}], "*", 
     RowBox[{"u", "^", "3"}]}], "+", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"453.70625272357658631`19.656774764508768", "*", "nu"}], "-", 
       RowBox[{"1081.8908931907688518`19.034183465137993", "*", 
        RowBox[{"nu", "^", "2"}]}], "+", 
       RowBox[{"602.31854041656388904`19.77982623171894", "*", 
        RowBox[{"nu", "^", "3"}]}]}], ")"}], "*", 
     RowBox[{"prstar", "^", "4"}], "*", 
     RowBox[{"u", "^", "4"}]}]}]}]}]], "Input",
 CellChangeTimes->{{3.8943497424507732`*^9, 3.894349758021113*^9}, {
  3.894350409505589*^9, 3.894350424742104*^9}, {3.89955112613911*^9, 
  3.8995511278551197`*^9}},ExpressionUUID->"6ba08c5f-2804-4b1b-9cf6-\
5349509f4d0f"],

Cell[BoxData[
 RowBox[{
  RowBox[{"2.`", " ", 
   RowBox[{"(", 
    RowBox[{"4.`", "\[VeryThinSpace]", "-", 
     RowBox[{"3.`", " ", "nu"}]}], ")"}], " ", "nu", " ", 
   SuperscriptBox["prstar", "4"], " ", 
   SuperscriptBox["u", "2"]}], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "2.783007636952232489`18.444514398098924"}], " ", "nu"}], 
     "-", 
     RowBox[{"5.4`", " ", 
      SuperscriptBox["nu", "2"]}], "+", 
     RowBox[{"6.`", " ", 
      SuperscriptBox["nu", "3"]}]}], ")"}], " ", 
   SuperscriptBox["prstar", "6"], " ", 
   SuperscriptBox["u", "2"]}], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     FractionBox[
      RowBox[{"6", " ", "nu"}], "7"], "+", 
     FractionBox[
      RowBox[{"18", " ", 
       SuperscriptBox["nu", "2"]}], "7"], "+", 
     FractionBox[
      RowBox[{"24", " ", 
       SuperscriptBox["nu", "3"]}], "7"], "-", 
     RowBox[{"6", " ", 
      SuperscriptBox["nu", "4"]}]}], ")"}], " ", 
   SuperscriptBox["prstar", "8"], " ", 
   SuperscriptBox["u", "2"]}], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"92.711044284955949757`19.96713147293964", " ", "nu"}], "-", 
     RowBox[{"131.`", " ", 
      SuperscriptBox["nu", "2"]}], "+", 
     RowBox[{"10.`", " ", 
      SuperscriptBox["nu", "3"]}]}], ")"}], " ", 
   SuperscriptBox["prstar", "4"], " ", 
   SuperscriptBox["u", "3"]}], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "21.096091643426789868`19.324202003405976"}], " ", "nu"}], 
     "-", 
     RowBox[{"78.6`", " ", 
      SuperscriptBox["nu", "2"]}], "+", 
     RowBox[{"188.`", " ", 
      SuperscriptBox["nu", "3"]}], "-", 
     RowBox[{"14.`", " ", 
      SuperscriptBox["nu", "4"]}]}], ")"}], " ", 
   SuperscriptBox["prstar", "6"], " ", 
   SuperscriptBox["u", "3"]}], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"453.70625272357658631`19.656774764508768", " ", "nu"}], "-", 
     RowBox[{"1081.8908931907688518`19.034183465137993", " ", 
      SuperscriptBox["nu", "2"]}], "+", 
     RowBox[{"602.31854041656388904`19.77982623171894", " ", 
      SuperscriptBox["nu", "3"]}]}], ")"}], " ", 
   SuperscriptBox["prstar", "4"], " ", 
   SuperscriptBox["u", "4"]}]}]], "Output",
 CellChangeTimes->{3.894349762070109*^9, 3.894349969532436*^9, 
  3.89435004496634*^9, 3.8943504277051477`*^9, 3.8943504625302277`*^9},
 CellLabel->
  "Out[165]=",ExpressionUUID->"d74e9e91-8649-4ded-9bea-831f40d51b49"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"Q", "[", 
         RowBox[{"u", ",", " ", "prstar"}], "]"}], "/.", 
        RowBox[{"prstar", "->", 
         RowBox[{"-", "0.1"}]}]}], "/.", 
       RowBox[{"nu", "->", "0.25"}]}], ")"}], ",", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"Qm", "[", 
         RowBox[{"u", ",", " ", "prstar"}], "]"}], "/.", 
        RowBox[{"prstar", "->", 
         RowBox[{"-", "0.1"}]}]}], "/.", 
       RowBox[{"nu", "->", "0.25"}]}], ")"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"u", ",", " ", "0.000000001", ",", " ", "1"}], "}"}], ",", 
   RowBox[{"AxesLabel", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"\"\<u\>\"", ",", " ", "\"\<A\>\""}], "}"}]}], ",", 
   RowBox[{"PlotRangePadding", "->", "Automatic"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.894344613615776*^9, 3.894344631463715*^9}, {
  3.894344781319009*^9, 3.8943448042563057`*^9}, {3.894345069167156*^9, 
  3.894345070444848*^9}, {3.894345453081655*^9, 3.894345454186096*^9}, {
  3.8943483587350073`*^9, 3.894348372942788*^9}, {3.894349359303822*^9, 
  3.8943493639996433`*^9}, {3.894349403935359*^9, 3.894349412360993*^9}, {
  3.894350027403906*^9, 3.8943500355791483`*^9}, {3.8943504405287247`*^9, 
  3.8943504610405397`*^9}},
 CellLabel->
  "In[166]:=",ExpressionUUID->"f6fffb46-64ce-494a-8003-625798b969d8"],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    TagBox[
     {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
      1.], LineBox[CompressedData["
1:eJwVlmc81v/fhpGfUETIvDIysktKyvfzlp0iK1nZWSEhpZDLiKzsvWVvkR0h
e4+yumyRcVFWSt3/+9H5Op4dz46T2/yR1gMyEhISbVISkv/ff5UnwzP+6KKm
7LhhnW8c0u++KdUvsErC2w+n0iqXw2USO461VLEqQEoqbpV1eVDGgN0yx51f
HS79HTXaN9+VCZUy/5TKqgOaAppSvUt0mKNZZDUJlz5Q8e2GOqXzYurBLfnm
/MZgyTBJXmV+GYsz+KVmHmsG4SfNN2FAHgt4RK8cxWoJjbfUjrKWNLBnfoKy
bclW8Cn8i9QAnSFmk3BDepfTFjIf6PUpp1tieiX6EvxZD6H88/2wMQlHTKX1
sfA9fkfYaXX5/sb8CcaxrnC6TccJYl162pxcPbHovcEwo9jHsOU7aMI14Iud
JL1/YveLMzSoOyS2sQVhfidWA0JZXeHajJtB0FI49ofpCTm/4RMgW3mdGRMY
g7lykeI/JLvB7Hm7lRy6JGxdKPRIl/AUtvyGJep70zHLy6zPiZzukO9qvHg+
/S02Ddl7AWbPwaO6lpX2dD6mo3rRhSvrBQCnF0euRAnWq9NIrFn0gHQyiqee
VBWYgslNe01+LxAjU8pyMK/CGmzHVlatX4K/84+bzqK1GFtdmcRHHW/w/qr1
B7k2YrpNe06hu94QTVOr5cb1EYtskynVj8UDsau17U9/K9bf5bPBJ+UDr9SL
M3l72jHqgU7hH198gORgwCqSrRtTGqW1+/DMF6RSz0vhWvswnwmdvCBWP8BX
vXS2XhrEPhASl3Xr/MB/Rmlvm30EO1yY5T1n6A/OkRp1DwLHsCur/BbE3/5w
Ox+XyyA2jjlv2mfUJ7+C+WTmwwd0U1jJz4qZACwAXKLuK85KELDvBwc4HUIA
HP7uqH7TO4uZH/NPXOd8DSW5SIOYtoilUvaM1zS/hm+xr0NOtC1jkzT0zP5m
QZDIvp7fQr+KnWG4d1eTLBjUBnQdeILXMC2WlChcVjA4K6srKkpsYmG4haFV
+RCYISzx5vttYd08gnTvF0PA04s2FVH9wOREq0LU+cPgZ3p+Ks58F/OS+N3N
1hEG+3MCYXVX97E6qRtU36zfQMasnwKN6C+MNWO5pUknHLgk6d2aXY6w6iyq
eb7scGDc2Pyd0/kPu5sjQhqyGw6q6tslAlykKKLQGfRiI8A+Xrgyqf8YEi+J
Mf6wHAEbTCtf4+T+Q31lNZ68UpFwZB2VXtxNgaje/63f+hIJjB8dbEjZqFFu
DfeU7vkoePD1F33V/AmkWK9w2PAsCkyVWVMcWmgQvjno6mvWaAiKmqg6WqRD
/YHWCRVO0SAbFc8SSn8acWgqHE51RMP7P+2BPmoMqGbuqF7MLQYstsJk3u0y
IYqCSQ69vhi4ZKHXc2TOjLSdqz3xvLHgF0M46p1jQZtkj2F0OBYaKW+tip/h
QDI9aulHgnGgKEH1YLEXh15HCZEK4ONAQ5GL2iSOE/HyLra4i8eDo16UCEkV
NzJQvKfEGZIAc7cNSZxO8KM8GslclYUE+HZYXaeJBNDuGB2l87VECDlx73GI
53kUbtXd1baSCA+VvfBGPMKIIJYrtAlJMF3Nap8cKIKE932DmeOSYM/n9JOg
P6KoPQBTs1NMBnYOPKltwAXEqMFWEpWcDEY9rLZE3YvIjGWftvFnMpx9zSRE
IiaB/uSVDZ7KTIG6tlukz39eQhLd53Sq/qbChOatwL+zUsg7kqSKcDcNfk0N
Ohz7cRX1GXxloixOA10BP1uGk9dQ5ZOeh8Wi6VBt9NCQ00gGTU1Ic8jpp8Px
Bhy1TySGyFBe72e/dFAd/wmNwwjdofAXJZlOh4L2uHudNbLIze7n1+jjGfDE
5djfmzY3UHK/WZjgpQzIO/foNw9ODn2PRUStoAy4wHJ08macPPIXOCjLvpoJ
rpWzck8HlVBRsJXZNctM+Cka/bi1WBkNE0fpB95kwg6ZsKhqhAriqil3PljO
BK/JBDqvh6qoXuWh5K2YLMDJ8Z/vf6SO5osmFmeas+DxornbdZ87iJJeJcZ1
PQvO3OnYW1PTQLoTvHsp8m/B2/bMtzBnTbRtS6je2n4L5nHTNpWr2qgFizid
yZYNZ15mEMRe6aBIegUHbflswE1yqcnw30WStfk876Oz4fRQlrPyY13kRvkk
9IVUDhBiBK/lSeojpWmBFRHTHEiRMGKWJ+oj5rJJOUJgDrhGn9WbLzZANfdk
D2Qnc2A4m7TURsoI/c45YUHhmQt8y2Xclz1NUO/zD43V2bngrcZk2yViipLV
H7PY9udCulWA65UUU4T2Pvf1cObBB9FzXN/9zJC3YubViJY8iHjv+DrKyQL9
t3iVloOyABRnYgb3m6zR5+o1m74LBTBmyxLcftcG5QSntnrpF8BCvbjR3U0b
pHKJ3H2uoABcEvQCxgTsUJDPwEKOWiEE3T/brtVojwx0fZCeWyHQznSnMNo5
IGGhywlUaYVg947LfprVEfUOJarbbxVCzojR5wyfR+gUt1XtxagieHs7lp4U
e4xmd1gYF+qLIGSKv+cg7TEq6+xxjF4sgkyZa4zx5M5Iy+ki7/7lYlCITWWf
HXZG0U1/whrGi4HIaPKVzc8VsRpHWSqfLQX1Hx+SnmLPkDrOxkpEqRQyxdzY
s+qeIZ9pGRt6x1IgFOLyyqTd0XeD5YdTjaXwqzv+nD/2HBEKk2urxcrgiHw7
OsnIA5XlEZtv9ZeBEI0jwcbLG6lSTt5A82Vwvo7QdLnPGy1at7Vc2CsD14ev
PdhxeMQqkNDGdLYcKDnllq8345FPtlznjEM53Nsaq2Nl9EU6mTEDLjQV8MCP
QzFlxx9tknprWHFXwEJIlthl3Vco0MxuSO9yBdQg5R662leokQtGsPsV8Pkf
A5bqE4D401Y+Hy+ugNPDYv6mfK/RQdJ1QtLtd7BvgIsiKQpBkb/5TMJM34FO
mUOIwLlQJGJIN+vt+g6KJuLpm5NCkRnb0tyD5HdwRykqVTMiDHXHhy2Jr7+D
5Ac4OT7lcJQUM7feGlwJkyRfC3G2kSjT6hqyzqiEwFeflk5XR6I8qag31NWV
8L9r4fT3vyhUOaEgoTlfCcLcOf7NOVGoF5f3lHC1CkT2g0MFd6LRn2xHsl9L
VRBv9qzMqDkOkT3t0E7+XQVca9s7RWfjEaUKVzbQvweuVvdKRq94xPh9SPmV
zHt4mOUsdUk2AYmKXQ5liHoP+0Sn1pWhRGT8/jezmGw1vDQMf8AlmoosA3Rs
h+5Ww33Zpf6NlFRkp1dc5/qwGlq+FKzvnkpDbofG9+tjq0HsXbVh034aeoNa
MlU2qoFa40fQ9bV01NweKGqRWAOuPqYRsquZiPsLk3z8Ti30HlWzDNvloj8z
4atdlHVQlNxz+fu7XDS+ciL8N0cddPXv5RYd5aKwQ9Kvxop1sKw6mCMZlYcO
cZtufLF18GvN5MChPR8NW7YXVEjVQ/GUcfMj9SJU7HBDc/FWPXwMhoP8zCIU
6Nawz2RaD+xvX9xK2y9CKLBCwT2wHnYXvcObsopRQWEqASbqIWSJ+csOeSny
+eF2uu95A/BGNbgx7JWh+7+3a/6GNQDj52OVqvzl6Cq5g8mFrAb442ijsKJb
jjaZLIqiehrgOPftFL3acmQgfUfJgKMRaH9f/oPzrUAS3gLPvzU2gtYEK1O9
UCWapxmfIyVtAnvCxvc7/dVIMPCj3uKZJth6dOdMHn0NciIrHGgXaYKxkWKn
sLs16O+eZ2OwXhOstVztpZypQayz5xKYyppA07xsq+ugFp2fkMqLFm8GPZfd
K2IqDeh2bNGC/7Vm4F8RcuyIbkCPtLk5nyo2w8ce5We/5xrQ+z7qWH3DZijO
eyQr5dWIFFq++p4NaIaU+zgjxg8fkGmhr0keoRkcv90QpC1pRn42B4kJq80Q
+YLe2HyiGeXyOXwO2mkG57rFcOr/PqLNNF01B+qPUGDXl6p3/yPyiBa8LnHl
I9xTzxXdp29BcZ4DZxpCP8Izi2TvlOBW1H+HvX9ApgWU9A4H4lrbUXeA3s9Q
5RagmqTck/3VjjqaYlhua7UAVvPWU0W8AzWL01l0WrdAzWGXtWtyByo/9d9B
c2QL8C6Tl9960YmiBza5K1ZawNGh97z+rW5keKflSXR0K9gfPyolv9qP9AL+
JWqltcJ3JNiv9bwf6TTJNNMVtMLyv3kXmsZ+dFu8mjq0qRUELiQIdcsPIHSq
KM3/eys8Y8TopYQHEc9ATJebbBvorMftWagMoRV1W5z+ehu4M1tYvyoZQQOZ
Ug7Ge20A/zhGLoyOoPd7/zVakHwCgatyx+gPR5BfaqaRI+MneP7f2RcuSqOI
c3MqyVfmE3SJX9NtnxtFuqHqbCUhnyCyV8xhkOczauu5xHxMtB2melz0ZgfG
USEnmTWlVDvQfRw6zPkzjiJdBt/T3GiH9bmzM/mCE8iU3UGX5W47TNuxyz/w
m0C/7XJiRD3bQTg/TqdGZhJJULMy6vW1Q7orScffmimUrvKXrtihA2gk6HqD
5ggIGzvWFfSsA+jmzQxq2GbQpBkV3sa3A4o1F0YkdWYQwwvG7XMJHXA8lTWX
vXMGvSoWGkps64CCV+13R/VnkcPpexGB7J1wnpDZ+O/2HLo2XUpv2dUJS+XU
HoNaC+iLTVXXjdFOMOUGv2TXBeS6W4fnnOmEBa/5hOjYBVRM2749udMJxddc
/KimFhDXjekhTa4uWM4jeUFttYiO51BGwtMuCN8klQvyW0Kjj8xOs/N1A+ml
ml9hi9+QIcvBDoh3g05p7JYv1Qqabw77YindDd4prS+zxFbQNn1DcolaN1hu
0ayYu68g2sozAnJu3TBOQWu3TLeKVA56rtt2dMMa1UQOh8p31Oh95UG1bQ8s
yO8eCI+vIwXBPuVplx7I+Vml+Y5s4399tRAi9eqB5LVZU1vRDTTJE0FUjeyB
xUfXBc19N9BB25o7oa4HDMo3GIIubiIJqoyw/072Qr62AsVQLBHlRJyo0S7p
BSeVO2VM8tuIosrOT7GmFw7FhM+tGWwjq/EuDamWXtBgf8My4byNBDgDV9k+
9wLhH67vb+Y2yisiZ58/6oUXnZ2iLuQ/UEHHkaeTWh/w2oub0/b+QCVHRPnQ
9T44xuap7ee4g2i579C93OuDm19WXge93kGOCiXTTiT9kGZgZVz4dgeJB9s/
0WHsh/M1tvzY1A4qY1nNYZfph4tRXkxZN3dRxaV5qoLgfqCITogPEN5DVbaj
A+1CA2DaEBQqSn6Aej/ZkUdcHoBm6UTiDb4DNM9NKm0oOwAi1DewZ0oHiHZS
NJN4dwBu76bvqb4+QFaqAa4s+AGwVjMwD6b7hViEr7HafRmAnMtwPYTvEGF7
d8op5AYhMYfU9ILdH5ReXXWYvzkIJ1c8DmPLSSDd5WZBxc9BeGxxnFrj0/9Y
/Kt+/cEgnOBeesw2QQIZueS1vWRDMCtX6LxAQgqZcdpPiWeGIE/tiaayJilk
P93+KQlDAOEDd012SaFASnSj6c0QqBgXJzHfOgZV1W9nxi4Mw3FcqeOaGAXQ
0em0ql4ZBqEl+5N6NynA3uZYbtP1YVAITWeetqAAHlZzx3ylYbiIoy0WSqCA
8OecRx5Gw+AtaKq9QH4c7LBENt7AYXAaGYjpnz0OZ1vf3HWeGQZWGSqyzQIq
COx/3kMbNgJsoW/YrIJpQMOSltcuagTKT8v3+hTSAMthhsen+BEwyAh+XttD
A7l8XaIeWSPw49Z98icnaaHdgzl8tWYEfjaaa/G/oQVy4UqdtoUR4AnwPeWV
cAq8Aza+ukuPgoqQiKrEHB24y5puLS6OQt+SoSGzJANcsZQhLKyOwt7c1alb
SgzwM4Cld35zFNR/sUmF6jGAw8Bg7uzBKAzNUiRd8WQAM+MbJtMnx8Cg0rOB
t4MBbnrw9I9IjsHDxrUKl/uMwFqzWNjiNwYN7bH8x6OZoFbc1jqd9zNkZQ9S
mQixQPVl/SWbl1/AU1nlhX7EWSCvta860zoOis/1ZBv7eeCUtnXIEPckJIas
DVxQFgBplcfJgs+n4MPT/Lv1ViJQXpf616p+GtTpg4T4dy7Aivw763JmAuQo
TjWbmUqCg+oaz5zRDGg0PTW1orgKlLoJJ76ozQLX26kxP3oZkI68QNV2OAsH
lqzORb6yQB86UXoueQ6Y8KltXzjlQYVhb4Lq1jx8vxC/OKauBLTKiekf9uaB
jmbajX/0JnhIa9Atxy4A45MaCzNvNbhZ7X4sWmERbuAOzsz80oDGn5VplcuL
0NvITRlzSxuavz4dz/JfgrMCxRkxQ3eBR7skmlViGch4Wh/mfdUDkYC9X6Qj
y0ASTVvgedwIYpQq5p3w38BVWY0jY9wEstteumrxrkDtbJ6LxIoZKG6NZpK1
rcCM2YsMwilLUMhN0ZF+vAqxknTFDPlW8J3jWen86e9QUZY0mmBoC0Rce+jp
lu+wshp/NjXZHnx7kyjpHddACy/Ue5LZCZqkfUV8aNeh0Zcy+NPWYzCQIIy4
N6yDYYbDJ3sRV0heVeVdNN4Ak87R1BYvN3BUB3rc3w2QVB22KiJ3hzSUi58u
2AT9CsK+PJ0HPN3gSqK9TYRU3HXOA+mXIBsbaEpQJ8IVnxidCZWXQAlbfCWa
RIh+8fDhx3svISG8qUztHhH4SWRUc568hPpLxu0h5kT4vJjA11r+Ev65J21T
uxPBiymBLofJGwIozqgczyECT+6rH+MR3qBR6knzJY8IkhexT3UJ3sCitzSc
U0iEAoNBmfQMb8jPr7yvVE4Ekduy3G7l3tBzW9vFv4EIFfn6m2pD3nAqKjz1
2AgRGiSdGEvp8TB+/cBiZIwIq8J+/3pY8ZC+aCKYNU4E39T4gnVuPFy8Il4p
RyACvjZbWVoCD9oTfV34VSKc7S/u+6eFBw6fy28014lw+BS3es0QD4tCKTrc
RCJ8eGU17G6Bhyce9jPNO0Tg9X7JdMwVD4hv9G34PhHeZ/Y7q3nggaL/up3p
4f/8EnffJvnhod8tS/zCERFKl6uKN0LwEMd5YvffPyJstQ+Fycfg4f8A2uAs
6A==
       "]]},
     Annotation[#, "Charting`Private`Tag$7824#1"]& ], 
    TagBox[
     {RGBColor[0.880722, 0.611041, 0.142051], AbsoluteThickness[1.6], Opacity[
      1.], LineBox[CompressedData["
1:eJwVlmc81v/fhpGfUETIvDIysktKyvfzlp0iK1nZWSEhpZDLiKzsvWVvkR0h
e4+yumyRcVFWSt3/+9H5Op4dz46T2/yR1gMyEhISbVISkv/ff5UnwzP+6KKm
7LhhnW8c0u++KdUvsErC2w+n0iqXw2USO461VLEqQEoqbpV1eVDGgN0yx51f
HS79HTXaN9+VCZUy/5TKqgOaAppSvUt0mKNZZDUJlz5Q8e2GOqXzYurBLfnm
/MZgyTBJXmV+GYsz+KVmHmsG4SfNN2FAHgt4RK8cxWoJjbfUjrKWNLBnfoKy
bclW8Cn8i9QAnSFmk3BDepfTFjIf6PUpp1tieiX6EvxZD6H88/2wMQlHTKX1
sfA9fkfYaXX5/sb8CcaxrnC6TccJYl162pxcPbHovcEwo9jHsOU7aMI14Iud
JL1/YveLMzSoOyS2sQVhfidWA0JZXeHajJtB0FI49ofpCTm/4RMgW3mdGRMY
g7lykeI/JLvB7Hm7lRy6JGxdKPRIl/AUtvyGJep70zHLy6zPiZzukO9qvHg+
/S02Ddl7AWbPwaO6lpX2dD6mo3rRhSvrBQCnF0euRAnWq9NIrFn0gHQyiqee
VBWYgslNe01+LxAjU8pyMK/CGmzHVlatX4K/84+bzqK1GFtdmcRHHW/w/qr1
B7k2YrpNe06hu94QTVOr5cb1EYtskynVj8UDsau17U9/K9bf5bPBJ+UDr9SL
M3l72jHqgU7hH198gORgwCqSrRtTGqW1+/DMF6RSz0vhWvswnwmdvCBWP8BX
vXS2XhrEPhASl3Xr/MB/Rmlvm30EO1yY5T1n6A/OkRp1DwLHsCur/BbE3/5w
Ox+XyyA2jjlv2mfUJ7+C+WTmwwd0U1jJz4qZACwAXKLuK85KELDvBwc4HUIA
HP7uqH7TO4uZH/NPXOd8DSW5SIOYtoilUvaM1zS/hm+xr0NOtC1jkzT0zP5m
QZDIvp7fQr+KnWG4d1eTLBjUBnQdeILXMC2WlChcVjA4K6srKkpsYmG4haFV
+RCYISzx5vttYd08gnTvF0PA04s2FVH9wOREq0LU+cPgZ3p+Ks58F/OS+N3N
1hEG+3MCYXVX97E6qRtU36zfQMasnwKN6C+MNWO5pUknHLgk6d2aXY6w6iyq
eb7scGDc2Pyd0/kPu5sjQhqyGw6q6tslAlykKKLQGfRiI8A+Xrgyqf8YEi+J
Mf6wHAEbTCtf4+T+Q31lNZ68UpFwZB2VXtxNgaje/63f+hIJjB8dbEjZqFFu
DfeU7vkoePD1F33V/AmkWK9w2PAsCkyVWVMcWmgQvjno6mvWaAiKmqg6WqRD
/YHWCRVO0SAbFc8SSn8acWgqHE51RMP7P+2BPmoMqGbuqF7MLQYstsJk3u0y
IYqCSQ69vhi4ZKHXc2TOjLSdqz3xvLHgF0M46p1jQZtkj2F0OBYaKW+tip/h
QDI9aulHgnGgKEH1YLEXh15HCZEK4ONAQ5GL2iSOE/HyLra4i8eDo16UCEkV
NzJQvKfEGZIAc7cNSZxO8KM8GslclYUE+HZYXaeJBNDuGB2l87VECDlx73GI
53kUbtXd1baSCA+VvfBGPMKIIJYrtAlJMF3Nap8cKIKE932DmeOSYM/n9JOg
P6KoPQBTs1NMBnYOPKltwAXEqMFWEpWcDEY9rLZE3YvIjGWftvFnMpx9zSRE
IiaB/uSVDZ7KTIG6tlukz39eQhLd53Sq/qbChOatwL+zUsg7kqSKcDcNfk0N
Ohz7cRX1GXxloixOA10BP1uGk9dQ5ZOeh8Wi6VBt9NCQ00gGTU1Ic8jpp8Px
Bhy1TySGyFBe72e/dFAd/wmNwwjdofAXJZlOh4L2uHudNbLIze7n1+jjGfDE
5djfmzY3UHK/WZjgpQzIO/foNw9ODn2PRUStoAy4wHJ08macPPIXOCjLvpoJ
rpWzck8HlVBRsJXZNctM+Cka/bi1WBkNE0fpB95kwg6ZsKhqhAriqil3PljO
BK/JBDqvh6qoXuWh5K2YLMDJ8Z/vf6SO5osmFmeas+DxornbdZ87iJJeJcZ1
PQvO3OnYW1PTQLoTvHsp8m/B2/bMtzBnTbRtS6je2n4L5nHTNpWr2qgFizid
yZYNZ15mEMRe6aBIegUHbflswE1yqcnw30WStfk876Oz4fRQlrPyY13kRvkk
9IVUDhBiBK/lSeojpWmBFRHTHEiRMGKWJ+oj5rJJOUJgDrhGn9WbLzZANfdk
D2Qnc2A4m7TURsoI/c45YUHhmQt8y2Xclz1NUO/zD43V2bngrcZk2yViipLV
H7PY9udCulWA65UUU4T2Pvf1cObBB9FzXN/9zJC3YubViJY8iHjv+DrKyQL9
t3iVloOyABRnYgb3m6zR5+o1m74LBTBmyxLcftcG5QSntnrpF8BCvbjR3U0b
pHKJ3H2uoABcEvQCxgTsUJDPwEKOWiEE3T/brtVojwx0fZCeWyHQznSnMNo5
IGGhywlUaYVg947LfprVEfUOJarbbxVCzojR5wyfR+gUt1XtxagieHs7lp4U
e4xmd1gYF+qLIGSKv+cg7TEq6+xxjF4sgkyZa4zx5M5Iy+ki7/7lYlCITWWf
HXZG0U1/whrGi4HIaPKVzc8VsRpHWSqfLQX1Hx+SnmLPkDrOxkpEqRQyxdzY
s+qeIZ9pGRt6x1IgFOLyyqTd0XeD5YdTjaXwqzv+nD/2HBEKk2urxcrgiHw7
OsnIA5XlEZtv9ZeBEI0jwcbLG6lSTt5A82Vwvo7QdLnPGy1at7Vc2CsD14ev
PdhxeMQqkNDGdLYcKDnllq8345FPtlznjEM53Nsaq2Nl9EU6mTEDLjQV8MCP
QzFlxx9tknprWHFXwEJIlthl3Vco0MxuSO9yBdQg5R662leokQtGsPsV8Pkf
A5bqE4D401Y+Hy+ugNPDYv6mfK/RQdJ1QtLtd7BvgIsiKQpBkb/5TMJM34FO
mUOIwLlQJGJIN+vt+g6KJuLpm5NCkRnb0tyD5HdwRykqVTMiDHXHhy2Jr7+D
5Ac4OT7lcJQUM7feGlwJkyRfC3G2kSjT6hqyzqiEwFeflk5XR6I8qag31NWV
8L9r4fT3vyhUOaEgoTlfCcLcOf7NOVGoF5f3lHC1CkT2g0MFd6LRn2xHsl9L
VRBv9qzMqDkOkT3t0E7+XQVca9s7RWfjEaUKVzbQvweuVvdKRq94xPh9SPmV
zHt4mOUsdUk2AYmKXQ5liHoP+0Sn1pWhRGT8/jezmGw1vDQMf8AlmoosA3Rs
h+5Ww33Zpf6NlFRkp1dc5/qwGlq+FKzvnkpDbofG9+tjq0HsXbVh034aeoNa
MlU2qoFa40fQ9bV01NweKGqRWAOuPqYRsquZiPsLk3z8Ti30HlWzDNvloj8z
4atdlHVQlNxz+fu7XDS+ciL8N0cddPXv5RYd5aKwQ9Kvxop1sKw6mCMZlYcO
cZtufLF18GvN5MChPR8NW7YXVEjVQ/GUcfMj9SJU7HBDc/FWPXwMhoP8zCIU
6Nawz2RaD+xvX9xK2y9CKLBCwT2wHnYXvcObsopRQWEqASbqIWSJ+csOeSny
+eF2uu95A/BGNbgx7JWh+7+3a/6GNQDj52OVqvzl6Cq5g8mFrAb442ijsKJb
jjaZLIqiehrgOPftFL3acmQgfUfJgKMRaH9f/oPzrUAS3gLPvzU2gtYEK1O9
UCWapxmfIyVtAnvCxvc7/dVIMPCj3uKZJth6dOdMHn0NciIrHGgXaYKxkWKn
sLs16O+eZ2OwXhOstVztpZypQayz5xKYyppA07xsq+ugFp2fkMqLFm8GPZfd
K2IqDeh2bNGC/7Vm4F8RcuyIbkCPtLk5nyo2w8ce5We/5xrQ+z7qWH3DZijO
eyQr5dWIFFq++p4NaIaU+zgjxg8fkGmhr0keoRkcv90QpC1pRn42B4kJq80Q
+YLe2HyiGeXyOXwO2mkG57rFcOr/PqLNNF01B+qPUGDXl6p3/yPyiBa8LnHl
I9xTzxXdp29BcZ4DZxpCP8Izi2TvlOBW1H+HvX9ApgWU9A4H4lrbUXeA3s9Q
5RagmqTck/3VjjqaYlhua7UAVvPWU0W8AzWL01l0WrdAzWGXtWtyByo/9d9B
c2QL8C6Tl9960YmiBza5K1ZawNGh97z+rW5keKflSXR0K9gfPyolv9qP9AL+
JWqltcJ3JNiv9bwf6TTJNNMVtMLyv3kXmsZ+dFu8mjq0qRUELiQIdcsPIHSq
KM3/eys8Y8TopYQHEc9ATJebbBvorMftWagMoRV1W5z+ehu4M1tYvyoZQQOZ
Ug7Ge20A/zhGLoyOoPd7/zVakHwCgatyx+gPR5BfaqaRI+MneP7f2RcuSqOI
c3MqyVfmE3SJX9NtnxtFuqHqbCUhnyCyV8xhkOczauu5xHxMtB2melz0ZgfG
USEnmTWlVDvQfRw6zPkzjiJdBt/T3GiH9bmzM/mCE8iU3UGX5W47TNuxyz/w
m0C/7XJiRD3bQTg/TqdGZhJJULMy6vW1Q7orScffmimUrvKXrtihA2gk6HqD
5ggIGzvWFfSsA+jmzQxq2GbQpBkV3sa3A4o1F0YkdWYQwwvG7XMJHXA8lTWX
vXMGvSoWGkps64CCV+13R/VnkcPpexGB7J1wnpDZ+O/2HLo2XUpv2dUJS+XU
HoNaC+iLTVXXjdFOMOUGv2TXBeS6W4fnnOmEBa/5hOjYBVRM2749udMJxddc
/KimFhDXjekhTa4uWM4jeUFttYiO51BGwtMuCN8klQvyW0Kjj8xOs/N1A+ml
ml9hi9+QIcvBDoh3g05p7JYv1Qqabw77YindDd4prS+zxFbQNn1DcolaN1hu
0ayYu68g2sozAnJu3TBOQWu3TLeKVA56rtt2dMMa1UQOh8p31Oh95UG1bQ8s
yO8eCI+vIwXBPuVplx7I+Vml+Y5s4399tRAi9eqB5LVZU1vRDTTJE0FUjeyB
xUfXBc19N9BB25o7oa4HDMo3GIIubiIJqoyw/072Qr62AsVQLBHlRJyo0S7p
BSeVO2VM8tuIosrOT7GmFw7FhM+tGWwjq/EuDamWXtBgf8My4byNBDgDV9k+
9wLhH67vb+Y2yisiZ58/6oUXnZ2iLuQ/UEHHkaeTWh/w2oub0/b+QCVHRPnQ
9T44xuap7ee4g2i579C93OuDm19WXge93kGOCiXTTiT9kGZgZVz4dgeJB9s/
0WHsh/M1tvzY1A4qY1nNYZfph4tRXkxZN3dRxaV5qoLgfqCITogPEN5DVbaj
A+1CA2DaEBQqSn6Aej/ZkUdcHoBm6UTiDb4DNM9NKm0oOwAi1DewZ0oHiHZS
NJN4dwBu76bvqb4+QFaqAa4s+AGwVjMwD6b7hViEr7HafRmAnMtwPYTvEGF7
d8op5AYhMYfU9ILdH5ReXXWYvzkIJ1c8DmPLSSDd5WZBxc9BeGxxnFrj0/9Y
/Kt+/cEgnOBeesw2QQIZueS1vWRDMCtX6LxAQgqZcdpPiWeGIE/tiaayJilk
P93+KQlDAOEDd012SaFASnSj6c0QqBgXJzHfOgZV1W9nxi4Mw3FcqeOaGAXQ
0em0ql4ZBqEl+5N6NynA3uZYbtP1YVAITWeetqAAHlZzx3ylYbiIoy0WSqCA
8OecRx5Gw+AtaKq9QH4c7LBENt7AYXAaGYjpnz0OZ1vf3HWeGQZWGSqyzQIq
COx/3kMbNgJsoW/YrIJpQMOSltcuagTKT8v3+hTSAMthhsen+BEwyAh+XttD
A7l8XaIeWSPw49Z98icnaaHdgzl8tWYEfjaaa/G/oQVy4UqdtoUR4AnwPeWV
cAq8Aza+ukuPgoqQiKrEHB24y5puLS6OQt+SoSGzJANcsZQhLKyOwt7c1alb
SgzwM4Cld35zFNR/sUmF6jGAw8Bg7uzBKAzNUiRd8WQAM+MbJtMnx8Cg0rOB
t4MBbnrw9I9IjsHDxrUKl/uMwFqzWNjiNwYN7bH8x6OZoFbc1jqd9zNkZQ9S
mQixQPVl/SWbl1/AU1nlhX7EWSCvta860zoOis/1ZBv7eeCUtnXIEPckJIas
DVxQFgBplcfJgs+n4MPT/Lv1ViJQXpf616p+GtTpg4T4dy7Aivw763JmAuQo
TjWbmUqCg+oaz5zRDGg0PTW1orgKlLoJJ76ozQLX26kxP3oZkI68QNV2OAsH
lqzORb6yQB86UXoueQ6Y8KltXzjlQYVhb4Lq1jx8vxC/OKauBLTKiekf9uaB
jmbajX/0JnhIa9Atxy4A45MaCzNvNbhZ7X4sWmERbuAOzsz80oDGn5VplcuL
0NvITRlzSxuavz4dz/JfgrMCxRkxQ3eBR7skmlViGch4Wh/mfdUDkYC9X6Qj
y0ASTVvgedwIYpQq5p3w38BVWY0jY9wEstteumrxrkDtbJ6LxIoZKG6NZpK1
rcCM2YsMwilLUMhN0ZF+vAqxknTFDPlW8J3jWen86e9QUZY0mmBoC0Rce+jp
lu+wshp/NjXZHnx7kyjpHddACy/Ue5LZCZqkfUV8aNeh0Zcy+NPWYzCQIIy4
N6yDYYbDJ3sRV0heVeVdNN4Ak87R1BYvN3BUB3rc3w2QVB22KiJ3hzSUi58u
2AT9CsK+PJ0HPN3gSqK9TYRU3HXOA+mXIBsbaEpQJ8IVnxidCZWXQAlbfCWa
RIh+8fDhx3svISG8qUztHhH4SWRUc568hPpLxu0h5kT4vJjA11r+Ev65J21T
uxPBiymBLofJGwIozqgczyECT+6rH+MR3qBR6knzJY8IkhexT3UJ3sCitzSc
U0iEAoNBmfQMb8jPr7yvVE4Ekduy3G7l3tBzW9vFv4EIFfn6m2pD3nAqKjz1
2AgRGiSdGEvp8TB+/cBiZIwIq8J+/3pY8ZC+aCKYNU4E39T4gnVuPFy8Il4p
RyACvjZbWVoCD9oTfV34VSKc7S/u+6eFBw6fy28014lw+BS3es0QD4tCKTrc
RCJ8eGU17G6Bhyce9jPNO0Tg9X7JdMwVD4hv9G34PhHeZ/Y7q3nggaL/up3p
4f/8EnffJvnhod8tS/zCERFKl6uKN0LwEMd5YvffPyJstQ+Fycfg4f8A2uAs
6A==
       "]]},
     Annotation[#, "Charting`Private`Tag$7824#2"]& ]}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox["\"u\"", TraditionalForm], 
    FormBox["\"A\"", TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, 
    "DefaultGraphicsInteraction" -> {
     "Version" -> 1.2, "TrackMousePosition" -> {True, False}, 
      "Effects" -> {
       "Highlight" -> {"ratio" -> 2}, "HighlightPoint" -> {"ratio" -> 2}, 
        "Droplines" -> {
         "freeformCursorMode" -> True, 
          "placement" -> {"x" -> "All", "y" -> "None"}}}}, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{1.*^-9, 1}, {0., 0.008585479541781876}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{
  3.8943497622242727`*^9, 3.894349969563319*^9, 3.894350045043499*^9, {
   3.8943504277664537`*^9, 3.894350462596738*^9}},
 CellLabel->
  "Out[166]=",ExpressionUUID->"98da1154-8dfd-466d-b35a-bfdd5e9b59a3"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{"Q", "[", 
         RowBox[{"u", ",", " ", "prstar"}], "]"}], "-", 
        RowBox[{"Qmatlab", "[", 
         RowBox[{"u", ",", " ", "prstar"}], "]"}]}], ")"}], "/.", 
      RowBox[{"prstar", "->", 
       RowBox[{"-", "0.1"}]}]}], "/.", 
     RowBox[{"nu", "->", "0.25"}]}], ")"}], ",", 
   RowBox[{"{", 
    RowBox[{"u", ",", " ", "0.000000001", ",", " ", "1"}], "}"}], ",", 
   RowBox[{"AxesLabel", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"\"\<u\>\"", ",", " ", "\"\<A\>\""}], "}"}]}], ",", 
   RowBox[{"PlotRangePadding", "->", "Automatic"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.894348572453156*^9, 3.89434859156495*^9}, {
  3.894349367751791*^9, 3.894349371239628*^9}, {3.8943494039393*^9, 
  3.894349403941484*^9}, {3.894350038971339*^9, 3.894350042931069*^9}, {
  3.899551135293454*^9, 
  3.899551136179351*^9}},ExpressionUUID->"b5e0e734-aab5-49ed-ae0e-\
e20494bd0353"],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    TagBox[
     {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
      1.], LineBox[CompressedData["
1:eJxFz30s1AEYB/BDE11eTkv9hor4g9lIqW4aFnEJ4W5KK0lK17xkKZ1p5m1K
rpSX4rxckZdaLrnbJYpxsaJT7nSndDrOy3E4SyorV231PM/27Nlnz55n+9pG
J4ad0ieRSEF/+u/U8dcW3P0Z7kn6V82Tfq1jxA6v/y7rMegUEL5g9q7ol5UE
A5ybSPEvJGLA2WR1LptIBmcOMerziGxwRkfe7qtEEXiBqRBqF2rARGRhjP8m
HphTrNR0XeODbWXrfe4stoBHTeRKPb12sPiglbh/Tyd4KphpE6ERgbm0FfNH
8T1gaeIJCyuH1+Dam+Sn9MY+sIAp7e926sd7oWC5Ye4t7oU1I4OuA+Ar4tRe
0+sSMMs7SqtSScEtLsxYrv17sNA9YvxMugy8qiVOYNklB5vRY/Pf2X4AU2lJ
5Y6pH8FNzypXTrcOY16f5timDQpwfMCMnfLoCNgovJQsC/qM/265GouW0RT2
EG9ruRJMW7c0ZHxgFGzqX8Z9sYROo4aYT5SMgfcLWQZFvirw8y/8Kv4EuuNT
irw6ZxxsR28sItwmwM65Sz/0JOhivyej5zImwfdF6clh9lPgfVrpPX0R2reu
gkFNUoOnrS/xRi2mwfM23WyLTnRWH8eIkjADbqdmOWeaasBH3BQSVhu6XB1g
r4qcBScEe1FsVtBVnnUZww/mwCmzWzimgfNg75IrUYpgtJGX1qExFF1a0P44
6BC6dXtkd340WsfiLKxhoXMNLWmra9EhvMsmsnr0xsPjA7UP0Q0N/GN+Teje
QPr5nDa0WWFBpYEELff4flIyiOaqjjtWy9Hbdrrw9yrQ9KE3rzLUaOtM9xuh
GrTKqYJhO4++kBY30rGI9nSQ1hR8QxuKPc5GLaPFF6tdXH+hb28mf9Xp0L8B
NHFAfA==
       "]]},
     Annotation[#, "Charting`Private`Tag$7873#1"]& ]}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox["\"u\"", TraditionalForm], 
    FormBox["\"A\"", TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  ImageSize->{393., Automatic},
  Method->{
   "DefaultBoundaryStyle" -> Automatic, 
    "DefaultGraphicsInteraction" -> {
     "Version" -> 1.2, "TrackMousePosition" -> {True, False}, 
      "Effects" -> {
       "Highlight" -> {"ratio" -> 2}, "HighlightPoint" -> {"ratio" -> 2}, 
        "Droplines" -> {
         "freeformCursorMode" -> True, 
          "placement" -> {"x" -> "All", "y" -> "None"}}}}, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{1.*^-9, 1}, {-1., 1.}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{
  3.894344676512368*^9, {3.894344782508595*^9, 3.894344804686697*^9}, 
   3.894344920072053*^9, {3.894345065889529*^9, 3.894345071369648*^9}, 
   3.894345105772942*^9, 3.8943454545793552`*^9, {3.894348168088915*^9, 
   3.894348202916698*^9}, 3.894348248559681*^9, {3.894348368434997*^9, 
   3.894348373840947*^9}, 3.894348450005726*^9, {3.894348539942773*^9, 
   3.894348553420034*^9}, 3.8943485924177628`*^9, 3.8943497622561407`*^9, 
   3.894349969595834*^9, 3.8943500450989733`*^9, 3.894350427795326*^9, 
   3.894350462624542*^9},
 CellLabel->
  "Out[167]=",ExpressionUUID->"1ae6cb0e-7731-4875-8200-8e47f51401f5"]
}, Open  ]]
},
WindowSize->{959.25, 504.},
WindowMargins->{{1.5, Automatic}, {Automatic, 0}},
TaggingRules-><|"TryRealOnly" -> False|>,
FrontEndVersion->"13.2 for Linux x86 (64-bit) (January 30, 2023)",
StyleDefinitions->"ReverseColor.nb",
ExpressionUUID->"e76d9117-790a-41f3-a585-a377abcedf7a"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 254, 4, 32, "Text",ExpressionUUID->"6083321c-3070-4b07-af27-0e9342481565"],
Cell[CellGroupData[{
Cell[837, 28, 4522, 132, 670, "Input",ExpressionUUID->"d961856c-f898-4cff-a192-b0ceba0db6ae"],
Cell[5362, 162, 517, 10, 36, "Output",ExpressionUUID->"08286be7-fa71-4fd8-bc19-04d0ea2d4a9f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5916, 177, 894, 19, 74, "Input",ExpressionUUID->"5078db12-c6e8-4022-bb2a-85cff913bc57"],
Cell[6813, 198, 2460, 72, 132, "Output",ExpressionUUID->"a93c59fe-606f-4205-af7f-823bfd7ddb8f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9310, 275, 2756, 79, 157, "Input",ExpressionUUID->"6ba08c5f-2804-4b1b-9cf6-5349509f4d0f"],
Cell[12069, 356, 2459, 72, 132, "Output",ExpressionUUID->"d74e9e91-8649-4ded-9bea-831f40d51b49"]
}, Open  ]],
Cell[CellGroupData[{
Cell[14565, 433, 1443, 35, 56, "Input",ExpressionUUID->"f6fffb46-64ce-494a-8003-625798b969d8"],
Cell[16011, 470, 16400, 293, 238, "Output",ExpressionUUID->"98da1154-8dfd-466d-b35a-bfdd5e9b59a3"]
}, Open  ]],
Cell[CellGroupData[{
Cell[32448, 768, 1028, 26, 56, "Input",ExpressionUUID->"b5e0e734-aab5-49ed-ae0e-e20494bd0353"],
Cell[33479, 796, 3268, 75, 259, "Output",ExpressionUUID->"1ae6cb0e-7731-4875-8200-8e47f51401f5"]
}, Open  ]]
}
]
*)

