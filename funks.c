#include "header.h"
#include <gsl/gsl_math.h>



//-----------------------------------------------------------------------------
// Main workflow functions
/* 
in every case, compute potentials and derivatives on grid of r values
then produce output according to what you need (comparisons, etc...)
*/
//-----------------------------------------------------------------------------

void work_with_a(double nu, int rvals, double rmin, double dr, double *r_array, double *u_array, double *filler_array)
{
  double *A         = malloc(sizeof(double) * rvals);
  double *dA        = malloc(sizeof(double) * rvals);
  double *d2A       = malloc(sizeof(double) * rvals);
  double *Aold      = malloc(sizeof(double) * rvals);
  double *dAold     = malloc(sizeof(double) * rvals);
  double *d2Aold    = malloc(sizeof(double) * rvals);
  double *num_A     = malloc(sizeof(double) * rvals);
  double *num_dA    = malloc(sizeof(double) * rvals);
  double *num_d2A   = malloc(sizeof(double) * rvals);



  for (int i=0; i<rvals; i++)
  {
    eob_metric_A5PNlogP33(r_array[i], nu, &A[i], &dA[i], &d2A[i]);
    eob_metric_A5PNlog(r_array[i], nu, &Aold[i], &dAold[i], &d2Aold[i]);

  }

  // raise(SIGINT);


  // derive in u
  D0_x_4(A, u_array, rvals, num_dA);
  char name[30] = "an_num_u_dA.txt";
  writegood("numerr", name, num_dA, dA, r_array, dr, rvals);

  D0_x_4(dA, u_array, rvals, num_d2A);
  strcpy(name,"an_num_u_d2A.txt");
  writegood("numerr", name, num_d2A, d2A, r_array, dr, rvals);

  strcpy(name,"A.txt");
  writegood("single", name, A, filler_array, r_array, dr, rvals);

  strcpy(name,"Aold.txt");
  writegood("single", name, Aold, filler_array, r_array, dr, rvals);

  strcpy(name,"new_old_dA.txt");
  writegood("reldiff", name, dA, dAold, r_array, dr, rvals);

  strcpy(name,"new_old_d2A.txt");
  writegood("reldiff", name, d2A, d2Aold, r_array, dr, rvals);

  strcpy(name,"new_old_A.txt");
  writegood("reldiff", name, A, Aold, r_array, dr, rvals);


  free(A);      
  free(dA);     
  free(d2A);   
  free(Aold);      
  free(dAold);     
  free(d2Aold);  
  free(num_A);  
  free(num_dA);
  free(num_d2A);

}

void work_with_d(double nu, int rvals, double rmin, double dr, double *r_array, double *u_array, double *filler_array)
{
    double *D           = malloc(sizeof(double) * rvals);
    double *dD          = malloc(sizeof(double) * rvals);
    double *d2D         = malloc(sizeof(double) * rvals);
    double *Dold        = malloc(sizeof(double) * rvals);
    double *dDold       = malloc(sizeof(double) * rvals);
    double *d2Dold      = malloc(sizeof(double) * rvals);

    double *num_D       = malloc(sizeof(double) * rvals);
    double *num_dD      = malloc(sizeof(double) * rvals);
    double *num_d2D     = malloc(sizeof(double) * rvals);



    for (int i=0; i<rvals; i++)
    {
      eob_metric_D5PNP32(r_array[i], nu, &D[i], &dD[i], &d2D[i]);
      eob_metric_D3PN(r_array[i], nu, &Dold[i], &dDold[i], &d2Dold[i]);

    }

    // derive in r
    D0_x_4(D, u_array, rvals, num_dD);
    char name[30] = "an_num_u_dD.txt";
    writegood("numerr", name, num_dD, dD, r_array, dr, rvals);

    D0_x_4(dD, u_array, rvals, num_d2D);
    strcpy(name,"an_num_u_d2D.txt");
    writegood("numerr", name, num_d2D, d2D, r_array, dr, rvals);

    strcpy(name,"D.txt");
    writegood("single", name, D, filler_array, r_array, dr, rvals);

    strcpy(name,"Dold.txt");
    writegood("single", name, Dold, filler_array, r_array, dr, rvals);

    strcpy(name, "new_old_dD.txt");
    writegood("reldiff", name, dD, dDold, r_array, dr, rvals);

    strcpy(name,"new_old_d2D.txt");
    writegood("reldiff", name, d2D, d2Dold, r_array, dr, rvals);

    strcpy(name,"new_old_D.txt");
    writegood("reldiff", name, D, Dold, r_array, dr, rvals);


    free(D);      
    free(dD);     
    free(d2D);
    free(Dold);      
    free(dDold);     
    free(d2Dold);   
    free(num_D);  
    free(num_dD);
    free(num_d2D);
}




void work_with_q(double nu, int rvals, double rmin, double dr, double *r_array, double *u_array, double *filler_array)
{


  // first define things to test derivatives in r (or u) along r

  double prstar = -0.1;
  double *Q                 = malloc(sizeof(double[rvals]));
  double (*dQ_du)           = malloc(sizeof(double[rvals]));
  double (*dQ_dprstar)      = malloc(sizeof(double[rvals]));
  double (*d2Q_du2)         = malloc(sizeof(double[rvals]));
  double (*d2Q_drdprstar)   = malloc(sizeof(double[rvals]));
  double (*d2Q_dprstar2)    = malloc(sizeof(double[rvals]));
  double (*d3Q_dr2dprstar)  = malloc(sizeof(double[rvals]));
  double (*d3Q_drdprstar2)  = malloc(sizeof(double[rvals]));
  double (*d3Q_dprstar3)    = malloc(sizeof(double[rvals]));

  double *Qold                 = malloc(sizeof(double[rvals]));
  double (*dQ_duold)           = malloc(sizeof(double[rvals]));
  double (*dQ_dprstarold)      = malloc(sizeof(double[rvals]));
  double (*d2Q_du2old)         = malloc(sizeof(double[rvals]));
  double (*d2Q_drdprstarold)   = malloc(sizeof(double[rvals]));
  double (*d2Q_dprstar2old)    = malloc(sizeof(double[rvals]));
  double (*d3Q_dr2dprstarold)  = malloc(sizeof(double[rvals]));
  double (*d3Q_drdprstar2old)  = malloc(sizeof(double[rvals]));
  double (*d3Q_dprstar3old)    = malloc(sizeof(double[rvals]));


  double (*num_dQ_du)         = malloc(sizeof(double[rvals]));
  double (*num_dQ_dr)         = malloc(sizeof(double[rvals]));
  double (*num_dQ_dprstar)    = malloc(sizeof(double[rvals]));
  double (*num_d2Q_du2)       = malloc(sizeof(double[rvals]));
  double (*num_d2Q_drdprstar) = malloc(sizeof(double[rvals]));
  double (*num_d2Q_dprstar2)  = malloc(sizeof(double[rvals]));
  double (*num_d3Q_dr2dprstar)= malloc(sizeof(double[rvals]));
  double (*num_d3Q_drdprstar2)= malloc(sizeof(double[rvals]));
  double (*num_d3Q_dprstar3)  = malloc(sizeof(double[rvals]));



  //compute all values of Q and analytical derivatives
  for (int i=0; i<rvals; i++)
  {
    eob_metric_Q5PNloc(r_array[i], prstar, nu, 
                      &(Q[i]), &dQ_du[i], &dQ_dprstar[i], 
                      &d2Q_du2[i], &d2Q_drdprstar[i], &d2Q_dprstar2[i],
                      &d3Q_dr2dprstar[i], &d3Q_drdprstar2[i], &d3Q_dprstar3[i]);

    eob_metric_Q3PN(r_array[i], prstar, nu, 
                      &(Qold[i]), &dQ_duold[i], &dQ_dprstarold[i], 
                      &d2Q_du2old[i], &d2Q_drdprstarold[i], &d2Q_dprstar2old[i],
                      &d3Q_dr2dprstarold[i], &d3Q_drdprstar2old[i], &d3Q_dprstar3old[i]);
  }



  // compute single numerical derivatives from analytical quantitities

  // derive in u
  D0_x_4(Q, u_array, rvals, num_dQ_du);
  char name[30] = "an_num_u_dQ_du.txt";
  writegood("numerr", name, num_dQ_du, dQ_du, r_array, dr, rvals);

  D0_x_4(dQ_du, u_array, rvals, num_d2Q_du2);
  strcpy(name, "an_num_u_d2Q_du2.txt");
  writegood("numerr", name, num_d2Q_du2, d2Q_du2, r_array, dr, rvals);


  strcpy(name, "new_old_Q.txt");
  writegood("reldiff", name, Q, Qold, r_array, dr, rvals);

  strcpy(name, "new_old_u_dQ_du.txt");
  writegood("reldiff", name, dQ_du, dQ_duold, r_array, dr, rvals);

  strcpy(name, "new_old_u_d2Q_du2.txt");
  writegood("reldiff", name, d2Q_du2, d2Q_du2old, r_array, dr, rvals);


  strcpy(name,"Q.txt");
  writegood("single", name, Q, filler_array, r_array, dr, rvals);

  strcpy(name,"Qold.txt");
  writegood("single", name, Qold, filler_array, r_array, dr, rvals);


  // derive in r
  D0(dQ_dprstar, dr, rvals, num_d2Q_drdprstar);
  strcpy(name,"an_num_r_d2Q_drdprstar.txt");
  writegood("numerr", name, num_d2Q_drdprstar, d2Q_drdprstar, r_array, dr, rvals);

  D0(d2Q_drdprstar, dr, rvals, num_d3Q_dr2dprstar);
  strcpy(name, "an_num_r_d3Q_dr2dprstar.txt");
  writegood("numerr", name, num_d3Q_dr2dprstar, d3Q_dr2dprstar, r_array, dr, rvals);

  D0(d2Q_dprstar2, dr, rvals, num_d3Q_drdprstar2);
  strcpy(name, "an_num_r_d3Q_drdprstar2.txt");
  writegood("numerr", name, num_d3Q_drdprstar2, d3Q_drdprstar2, r_array, dr, rvals);


  strcpy(name,"new_old_r_d2Q_drdprstar.txt");
  writegood("reldiff", name, d2Q_drdprstar, d2Q_drdprstarold, r_array, dr, rvals);

  strcpy(name, "new_old_r_d3Q_dr2dprstar.txt");
  writegood("reldiff", name, d3Q_dr2dprstar, d3Q_dr2dprstarold, r_array, dr, rvals);

  strcpy(name, "new_old_r_d3Q_drdprstar2.txt");
  writegood("reldiff", name, d3Q_drdprstar2, d3Q_drdprstar2old, r_array, dr, rvals);



  // then we do test of derivatives in prstar along prstar
  double r = 30.;

  int prstarvals  = 10000;
  double prstarmin = -9.1;
  double dprstar   = 1e-3;


  double *prstar_array    = malloc(sizeof(double) * prstarvals);


  for (int i=0; i < prstarvals; i++)
  {
    prstar_array[i] = dprstar * i + prstarmin;
  }

  //compute all values of Q and analytical derivatives
  for (int i=0; i<prstarvals; i++)
  {
    eob_metric_Q5PNloc(r, prstar_array[i], nu, 
                      &(Q[i]), &dQ_du[i], &dQ_dprstar[i], 
                      &d2Q_du2[i], &d2Q_drdprstar[i], &d2Q_dprstar2[i],
                      &d3Q_dr2dprstar[i], &d3Q_drdprstar2[i], &d3Q_dprstar3[i]);


    eob_metric_Q3PN(r, prstar_array[i], nu, 
                      &(Qold[i]), &dQ_duold[i], &dQ_dprstarold[i], 
                      &d2Q_du2old[i], &d2Q_drdprstarold[i], &d2Q_dprstar2old[i],
                      &d3Q_dr2dprstarold[i], &d3Q_drdprstar2old[i], &d3Q_dprstar3old[i]);
  }


  D0(Q, dprstar, prstarvals, num_dQ_dprstar);
  strcpy(name,"an_num_p_dQ_dprstar.txt");
  writegood("numerr", name, num_dQ_dprstar, dQ_dprstar, prstar_array, dprstar, prstarvals);

  D0(dQ_dprstar, dprstar, prstarvals, num_d2Q_dprstar2);
  strcpy(name,"an_num_p_d2Q_dprstar2.txt");
  writegood("numerr", name, num_d2Q_dprstar2, d2Q_dprstar2, prstar_array, dprstar, prstarvals);

  D0(d2Q_dprstar2, dprstar, prstarvals, num_d3Q_dprstar3);
  strcpy(name,"an_num_p_d3Q_dprstar3.txt");
  writegood("numerr", name, num_d3Q_dprstar3, d3Q_dprstar3, prstar_array, dprstar, prstarvals);

  D0(d2Q_drdprstar, dprstar, prstarvals, num_d3Q_drdprstar2);
  strcpy(name, "an_num_p_d3Q_drdprstar2.txt");
  writegood("numerr", name, num_d3Q_drdprstar2, d3Q_drdprstar2, prstar_array, dprstar, prstarvals);


  strcpy(name,"new_old_p_Q.txt");
  writegood("reldiff", name, Q, Qold, prstar_array, dprstar, prstarvals);

  strcpy(name,"p_Q.txt");
  writegood("single", name, Qold, filler_array, prstar_array, dprstar, prstarvals);

  strcpy(name,"p_Qold.txt");
  writegood("single", name, Q, filler_array, prstar_array, dprstar, prstarvals);


  free(prstar_array);

  free(Q); 
  free(dQ_du);         
  free(dQ_dprstar);    
  free(d2Q_du2);   
  free(d2Q_drdprstar); 
  free(d2Q_dprstar2);
  free(d3Q_dr2dprstar);
  free(d3Q_drdprstar2);
  free(d3Q_dprstar3); 

  free(Qold); 
  free(dQ_duold);         
  free(dQ_dprstarold);    
  free(d2Q_du2old);   
  free(d2Q_drdprstarold); 
  free(d2Q_dprstar2old);
  free(d3Q_dr2dprstarold);
  free(d3Q_drdprstar2old);
  free(d3Q_dprstar3old); 

  free(num_dQ_du);
  free(num_dQ_dr); 
  free(num_dQ_dprstar);    
  free(num_d2Q_du2);       
  free(num_d2Q_drdprstar); 
  free(num_d2Q_dprstar2);  
  free(num_d3Q_dr2dprstar);
  free(num_d3Q_drdprstar2);
  free(num_d3Q_dprstar3);  

}



//-----------------------------------------------------------------------------
// Utility functions
//-----------------------------------------------------------------------------

// 
void writegood(char *instruction,
                char *name, double *numerical, double *analytical,
                double *x_array, double dx, int array_len)
{
  char relativepath[50] = "./outdir/";
  strcat(relativepath, name);
  FILE *punt_f = fopen(relativepath, "w");
  if (punt_f == NULL)
  {
      printf("Error while opening file %s \n", relativepath);
      exit(EXIT_FAILURE);
  }


  // for when we only want to plot one function
  // will have to plot only first array passed 
  if (!strcmp(instruction, "single"))
  {
    for (int i=0; i<array_len; i++)
    {
      fprintf(punt_f, "%f, %f", x_array[i], numerical[i]);
      fprintf(punt_f, "\n");
    }
  }
  // test discrepancy of analytical derivatives and finite differences derivatives
  // compared to expected order of numerical error
  // NB ADJUST ORDER ACCORDING TO FINITE DIFFERENCE SCHEME USED
  else if (!strcmp(instruction, "numerr"))
  {
    double dx4 = dx*dx*dx*dx;
    for (int i=0; i<array_len; i++)
    {
      fprintf(punt_f, "%f, %f", x_array[i], (analytical[i]-numerical[i])/dx4 );
      fprintf(punt_f, "\n");
    }
  }
  // test relative discrepancy of two expressions
  // ( not necessarily analytitcal and numerical derivatives)
  else if (!strcmp(instruction, "reldiff"))
  {
    for (int i=0; i<array_len; i++)
    {
      fprintf(punt_f, "%f, %f", x_array[i], (analytical[i]-numerical[i])/analytical[i] );
      fprintf(punt_f, "\n");
    }
  }
  else
  {
    printf("%s is a bad instruction to writegood\n", instruction);
    exit(EXIT_FAILURE);
  }
}

//-----------------------------------------------------------------------------
// FUNCTIONS FOR FINITE DIFFERENCES TAKEN FROM TEOB C SOURCE 

//-----------------------------------------------------------------------------

/** 4th order centered stencil first derivative, uniform grids */
int D0(double *f, double dx, int n, double *df)
{
  const double oo12dx  = 1./(12*dx);
  int i;
  for (i=2; i<n-2; i++) {
    df[i] = (8.*(f[i+1]-f[i-1]) - f[i+2] + f[i-2])*oo12dx;
  }
  i = 0;
  df[i] = (-25.*f[i] + 48.*f[i+1] - 36.*f[i+2] + 16.*f[i+3] - 3.*f[i+4])*oo12dx;
  i = 1;
  df[i] = (-3.*f[i-1] - 10.*f[i] + 18.*f[i+1] - 6.*f[i+2] + f[i+3])*oo12dx;
  i = n-2;
  df[i] = - (-3.*f[i+1] - 10.*f[i] + 18.*f[i-1] - 6.*f[i-2] + f[i-3])*oo12dx;
  i = n-1;
  df[i] = - (-25.*f[i] + 48.*f[i-1] - 36.*f[i-2] + 16.*f[i-3] - 3.*f[i-4])*oo12dx;
  return 1;
}

/** 4th order centered stencil second derivative, uniform grids */
int D2(double *f, double dx, int n, double *d2f)
{
  const double oo12dx2  = 1./(dx*dx*12);
  int i;
  for (i=2; i<n-2; i++) {
    d2f[i] = (-30*f[i]+16*(f[i+1]+f[i-1])-(f[i+2]+f[i-2]))*oo12dx2;
  }
  i= 0;
  d2f[i] = (45*f[i]-154*f[i+1]+214*f[i+2]-156*f[i+3]+61*f[i+4]-10*f[i+5])*oo12dx2;
  i= 1;
  d2f[i] = (10*f[i-1]-15*f[i]-4*f[i+1]+14*f[i+2]-6*f[i+3]+f[i+4])*oo12dx2;
  i = n-2;
  d2f[i] = (10*f[i+1]-15*f[i]-4*f[i-1]+14*f[i-2]-6*f[i-3]+f[i-4])*oo12dx2;
  i = n-1;
  d2f[i] = (45*f[i]-154*f[i-1]+214*f[i-2]-156*f[i-3]+61*f[i-4]-10*f[i-5])*oo12dx2;
  return 1;
}

/** 4th order first derivative, nonuniform grid */
/** FIXME: can be further optimized! */
int D0_x_4(double *f, double *x, int n, double *df)
{
  double *ix = x;
  double *iy = f;

  /* left boundary */
  df[0] = d4(ix, iy, 0);
  df[1] = d4(ix, iy, 1);

  /* central stencil */
  for(int k = 2; k < n-2; k++){
    df[k] = d4(ix, iy, 2);
    ix++; iy++;
  }
  ix--;iy--;
  /*right boundary*/
  df[n-2] = d4(ix, iy, 3);
  df[n-1] = d4(ix, iy, 4);
  
  return 1;
}

double d4(double *x, double *y, int i)
{
  double dy_i =0.; 
  double ld_j;

  for (int j = 0; j < 5; j++){
    ld_j = l_deriv(x, i, j);
    dy_i = *(y+j) *ld_j + dy_i;
  }
  return dy_i;
}

double l_deriv(double *x,int i,int j)
{
  double xi = *(x +i);
  double xj = *(x +j);
  double ld_ji;
  /*compute denominator*/
  double D = 1.;
  for (int a = 0; a<5; a++){
    if (a != j) D = D*(xj - *(x +a));
  }
  /*compute N and N/D */
  double N = 1.;
  if (i != j){                            //case 1: i != j
    for (int b = 0; b < 5 ; b++){
      if (b != j && b!= i) 
        N = N*(xi - *(x +b));
    }
    ld_ji = N/D;
  } else {                                //case 2: 1 ==j
    double tmp = 0.;
    for (int c = 0; c < 5; c++){
      double N = 1;
      if (c != j){
        for (int b = 0; b < 5; b++){
          if (b !=j && b != c) N = N*(xj - *(x + b));
        }
      tmp = tmp + N;  
      }
    }
    ld_ji = tmp/D;
  }

  return ld_ji;
}


