

EXEC = dtest

CC = gcc
CFLAGS = -g -fno-omit-frame-pointer

LIBS = -lgsl -lgslcblas -lm
LIBDIR = /usr/lib/

HEADERS = header.h
OBJS = main.o funks.o potentials.o 

%.o: %.c $(HEADERS) Makefile
	$(CC) $(CFLAGS) -c $< -o $@

$(EXEC): $(OBJS) Makefile
	${CC} $(CFLAGS) -o $(EXEC) $(OBJS) -L$(LIBDIR) $(LIBS)

clean:
	@rm -f $(OBJS) $(EXEC) *~make
