Various tests for a translation from Matlab to C of a new resummation of metric potential in the `TEOBResumS` GW waveform model.


Numerical tests for potential and their derivatives and analytical comparisons with Mathematica
